note
	description: "Game engine to show text with style and alignment."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

class
	ENGINE

inherit
	GAME_LIBRARY_SHARED
	PANGO_CONSTANTS

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization of `Current'
		local
			l_builder: GAME_WINDOW_RENDERED_BUILDER
		do
			create l_builder
			l_builder.set_dimension (Window_dimension_width, Window_dimension_height)
			l_builder.enable_resizable
			window := l_builder.generate_window
			has_error := window.has_error

			create {ARRAYED_LIST[ICON_BUTTON]}icons.make (8)
			create text_area.make(window.renderer, window.renderer.output_size.width,
						window.renderer.output_size.height - 50)
			has_error := has_error or text_area.has_error
			if not has_error then
				intialize_icon_textures
			end
		end

	intialize_icon_textures
			-- Initialize `icons'
		do
			icons.extend (create {LEFT_ICON_BUTTON}.make (window.renderer, 10, 5))
			icons.extend (create {CENTER_ICON_BUTTON}.make (window.renderer, 60, 5))
			icons.extend (create {RIGHT_ICON_BUTTON}.make (window.renderer, 110, 5))
			icons.extend (create {JUSTIFIED_ICON_BUTTON}.make (window.renderer, 160, 5))
			icons.extend (create {BOLD_ICON_BUTTON}.make (window.renderer, 210, 5))
			icons.extend (create {ITALIC_ICON_BUTTON}.make (window.renderer, 260, 5))
		end

feature -- Access

	has_error:BOOLEAN
			-- An error occured while initializing `Current'

	run
			-- Execute `Current'
		require
			No_error: not has_error
		do
			window.expose_actions.extend (agent on_expose)
			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend(agent on_key_pressed)
			window.mouse_wheel_move_actions.extend (agent on_mouse_wheel)
			window.mouse_button_released_actions.extend (agent on_mouse_pressed)
			window.resize_actions.extend (agent on_resize)
			text_area.update_texture(text)
			update_scene
			game_library.launch
		end

	update_scene
			-- Show modification of `Current' on `window'
		do
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (255, 255, 255))
			window.renderer.clear
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (0, 0, 0))
			window.renderer.draw_filled_rectangle (0, 0, window.renderer.output_size.width, 50)
			window.renderer.draw_sub_texture (
									text_area.texture, 0, text_area.start_y,
									text_area.width, text_area.height, 0, 50
								)
			across icons as la_icons loop
				window.renderer.draw_texture (la_icons.item.texture, la_icons.item.x, la_icons.item.y)
			end
			window.renderer.present
		end

	icons:LIST[ICON_BUTTON]
			-- Every icon buttons

	text_area:TEXT_AREA
			-- The area containig the `text'

	window:GAME_WINDOW_RENDERED
			-- The {GAME_WINDOW} to draw the scene

feature -- Constants

	Window_dimension_width:INTEGER = 600
			-- The initial horizontal dimension of the `window'

	Window_dimension_height:INTEGER = 600
			-- The initial vertical dimension of the `window'


	Text:READABLE_STRING_GENERAL
			-- Some single lines of text to show
		once
			Result :=
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fe" +
				"lis leo, auctor ac magna eget, luctus accumsan purus. Praesent aucto" +
				"r mollis diam in congue. Curabitur vitae elit et felis rhoncus sagit" +
				"tis. Donec mollis libero non sapien hendrerit maximus. Morbi turpis " +
				"ligula, vestibulum ac ultricies molestie, fermentum sed orci. Morbi " +
				"in commodo ante. Sed aliquam id ex eu molestie. Nunc cursus augue ve" +
				"lit, nec condimentum enim pulvinar eu. Pellentesque urna neque, iacu" +
				"lis ut mi ac, dignissim eleifend magna. Donec at eleifend nulla. Nul" +
				"lam tincidunt venenatis sem, vel auctor ex vulputate tincidunt. Ut l" +
				"aoreet vitae urna eu mattis. Pellentesque eu euismod nibh, in sodale" +
				"s mi. Sed tempor, purus sed mollis dapibus, ligula sapien varius sem" +
				", sit amet dignissim nisi nisi at arcu.%N%N" +
				"In eget nisi eros. Duis orci nibh, tincidunt non pretium sit amet, e" +
				"leifend vitae eros. Nullam sed sodales risus. Integer cursus tincidu" +
				"nt lorem, nec consequat sem vulputate sit amet. Fusce fringilla lore" +
				"m eu lacus convallis cursus. Curabitur massa massa, pulvinar venenat" +
				"is semper in, auctor vestibulum quam. In molestie, eros quis ferment" +
				"um rutrum, ex mi pharetra lacus, nec mattis dui lacus sed ex. Ut mag" +
				"na orci, interdum sed massa nec, tempus finibus odio. Suspendisse le" +
				"o mauris, ornare vitae ornare volutpat, finibus nec massa. Donec non" +
				" neque ut orci semper placerat. Integer lacinia, tortor at ornare fe" +
				"rmentum, erat odio convallis ex, sit amet elementum ligula lacus a n" +
				"ulla. Fusce auctor porttitor rutrum. Suspendisse laoreet tristique n" +
				"ulla, ut pharetra felis tincidunt non. Donec eu imperdiet est. Proin" +
				" ut cursus purus.%N%N" +
				"Duis et nunc vel quam gravida bibendum. Nunc malesuada ante a ex com" +
				"modo vestibulum ut sed arcu. Donec id magna quis purus lobortis bibe" +
				"ndum. Morbi ac ligula sit amet risus convallis dapibus. Aenean aliqu" +
				"am mauris massa, malesuada euismod sapien laoreet ac. In vel maximus" +
				" urna. Quisque semper dolor vitae nibh semper tincidunt. Fusce aucto" +
				"r, neque ac blandit laoreet, metus risus porttitor ligula, at alique" +
				"t elit dui nec velit. Lorem ipsum dolor sit amet, consectetur adipis" +
				"cing elit. Phasellus iaculis, libero eget faucibus pulvinar, risus e" +
				"st efficitur dui, sed scelerisque dolor elit sit amet purus.%N%N" +
				"Aenean a efficitur justo, a pharetra velit. Nulla consequat, massa q" +
				"uis congue rhoncus, velit justo lobortis elit, eget tincidunt orci m" +
				"assa sed mi. Morbi odio dolor, dignissim tempus metus sit amet, faci" +
				"lisis ultricies nisl. Ut sit amet arcu mauris. Ut in felis erat. Ut " +
				"porttitor, lacus nec tempor pulvinar, ipsum velit placerat augue, ut" +
				" imperdiet justo dui quis metus. Nam metus nisi, fermentum et malesu" +
				"ada sed, placerat quis nunc. Pellentesque sed eros nec elit rutrum m" +
				"ollis.%N%N" +
				"Etiam euismod hendrerit urna, ac sollicitudin magna. Aliquam tincidu" +
				"nt, arcu in hendrerit molestie, odio enim faucibus purus, et ultrici" +
				"es leo ante vel ipsum. Phasellus nisl arcu, sodales at luctus vitae," +
				" maximus ut metus. Donec nec efficitur ipsum, ac porttitor nisi. Ut " +
				"mattis nibh finibus turpis pulvinar pharetra. Pellentesque quis maur" +
				"is venenatis, blandit ipsum et, dictum mi. Phasellus sodales ante fa" +
				"cilisis erat venenatis, id faucibus arcu cursus. Aenean interdum eli" +
				"t enim, et sollicitudin nisi tempor id. Donec dapibus tempus luctus." +
				" Proin facilisis leo quis molestie ullamcorper."
		end

feature {NONE} -- Implementation

	on_mouse_pressed(a_timestamp:NATURAL_32; a_mouse_state:GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks:NATURAL_8)
			-- When the user press on a mouse button
		local
			l_icon:detachable ICON_BUTTON
		do
			if a_mouse_state.is_left_button then
				across icons as la_icons loop
					if
						a_mouse_state.x > la_icons.item.x and a_mouse_state.y > la_icons.item.y and
						a_mouse_state.x < la_icons.item.x + la_icons.item.texture.width and
						a_mouse_state.y < la_icons.item.y + la_icons.item.texture.height
					then
						l_icon := la_icons.item
					end
				end
				if attached {LEFT_ICON_BUTTON} l_icon as la_icon then
					text_area.set_alignment_left
				elseif attached {CENTER_ICON_BUTTON} l_icon as la_icon then
					text_area.set_alignment_center
				elseif attached {RIGHT_ICON_BUTTON} l_icon as la_icon then
					text_area.set_alignment_right
				elseif attached {JUSTIFIED_ICON_BUTTON} l_icon as la_icon then
					text_area.set_alignment_justified
				elseif attached {BOLD_ICON_BUTTON} l_icon as la_icon then
					text_area.change_weight
				elseif attached {ITALIC_ICON_BUTTON} l_icon as la_icon then
					text_area.toggle_italic
				end
				if attached l_icon then
					text_area.update_texture(text)
					update_scene
				end
			end
		end

	on_mouse_wheel(a_timestamp:NATURAL_32; a_mouse_state:GAME_MOUSE_EVENT; a_delta_x, a_delta_y:INTEGER_32)
			-- When the user scroll with the mouse wheel
		do
			text_area.scroll_text(a_delta_y * 10)
			update_scene
		end

	on_key_pressed(a_timestamp:NATURAL_32; a_key_state:GAME_KEY_EVENT)
			-- When the user pressed a key
		do
			if a_key_state.is_down then
				text_area.scroll_text (10)
				update_scene
			elseif a_key_state.is_up then
				text_area.scroll_text(-10)
				update_scene
			end
		end

	on_quit(a_timestamp:NATURAL_32)
			-- When he user close the `window'
		do
			game_library.stop
		end

	on_expose(a_timestamp:NATURAL_32)
			-- Redraw the scene when needed
		do
			update_scene
		end

	on_resize(a_timestamp:NATURAL_32; a_width, a_height:INTEGER_32)
			-- When the user resize `Current'
		do
			text_area.set_height (a_height - 50)
			text_area.set_width (a_width)
			text_area.update_texture(text)
			update_scene
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
