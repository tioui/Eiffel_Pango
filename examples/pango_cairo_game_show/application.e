note
	description: "An example of using Pango in an interactive game."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

class
	APPLICATION

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_engine:ENGINE
		do
			game_library.enable_video
			image_file_library.enable_png
			create l_engine.make
			if not l_engine.has_error then
				l_engine.run
			end
			game_library.clear_all_events
			game_library.quit_library
		end


note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
