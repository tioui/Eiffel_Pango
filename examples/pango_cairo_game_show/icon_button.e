note
	description: "Represent an icon button."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

deferred class
	ICON_BUTTON

inherit
	IMAGE


feature {NONE} -- Initialization

	make(a_name:READABLE_STRING_GENERAL; a_renderer:GAME_RENDERER; a_x, a_y:INTEGER)
			-- Initialization of `Current' using `a_name' to get the `texture'
		local
			l_image:IMG_IMAGE_FILE
		do
			create l_image.make (a_name + ".png")
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create texture.make_from_image (a_renderer, l_image)
					has_error := texture.has_error
				else
					create texture.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
					has_error := True
				end
			else
				create texture.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
				has_error := True
			end
			x := a_x
			y := a_y
		ensure
			X_Assign: x ~ a_x
			Y_Assign: y ~ a_y
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"


end
