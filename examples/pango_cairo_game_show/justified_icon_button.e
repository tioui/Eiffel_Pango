note
	description: "An icon button to represent justified alignment."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

class
	JUSTIFIED_ICON_BUTTON

inherit
	ICON_BUTTON
		rename
			make as make_button
		end

create
	make

feature {NONE} -- Initialization

	make(a_renderer:GAME_RENDERER; a_x, a_y:INTEGER)
			-- Initialization of `Current' using `a_renderer' to create the `texture'
		do
			make_button("justified", a_renderer, a_x, a_y)
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
