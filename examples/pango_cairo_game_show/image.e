note
	description: "A visual positionnable object with dimension."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

deferred class
	IMAGE

feature -- Access

	has_error:BOOLEAN
			-- An error occured while initialising `Current'

	texture:GAME_TEXTURE
			-- The image of `Current'

	x:INTEGER assign set_x
			-- Horizontal position of `Current'

	set_x(a_x:INTEGER)
			-- Assign `x' with the value of `a_x'
		do
			x := a_x
		ensure
			Is_Assign: x ~ a_x
		end

	y:INTEGER assign set_y
			-- Vertical position of `Current'

	set_y(a_y:INTEGER)
			-- Assign `y' with the value of `a_y'
		do
			y := a_y
		ensure
			Is_Assign: y ~ a_y
		end

	width:INTEGER
			-- horizontal dimension of `Current'
		do
			Result := texture.width
		end

	height:INTEGER
			-- vertical dimension of `Current'
		do

			Result := texture.height
		end



note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
