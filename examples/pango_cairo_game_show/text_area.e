note
	description: "A (read only) text area widget for Eiffel Game2 using Eiffel Cairo and Eiffel Pango."
	author: "Louis Marchand"
	date: "Sun, 01 Jul 2018 01:51:37 +0000"
	revision: "0.1"

class
	TEXT_AREA

inherit
	IMAGE
		redefine
			width, height
		end
	PANGO_CONSTANTS

create
	make

feature {NONE} -- Initialization

	make(a_renderer:GAME_RENDERER; a_width, a_height:INTEGER)
			-- Initialization of `Current' using `a_renderer'
			-- as `renderer' and of dimension `a_width' x `a_height'
		require
			Width_Positive: a_width > 0
			Height_Positive: a_height > 0
		do
			renderer := a_renderer
			width := a_width
			height := a_height
			start_y := 0
			create font_description.make_from_text (Font_description_text)
			has_error := not font_description.exists
			create texture.make (renderer, pixel_format, 1, 1)
		ensure
			Renderer_Is_Assign: renderer ~ a_renderer
			Width_Is_Assign: width ~ a_width
			Height_Is_Assign: height ~ a_height
		end


feature -- Access

	renderer:GAME_RENDERER
			-- {GAME_RENDERER} used to create `texture'

	update_texture(a_text:READABLE_STRING_GENERAL)
			-- Show modification of `Current' on `window'
		local
			l_surface:CAIRO_SURFACE_IMAGE
			l_height, l_width:INTEGER
			l_texture:GAME_TEXTURE_CAIRO
		do
			l_width := width
			create l_surface.make (create {CAIRO_PIXEL_FORMAT}.make_argb32, l_width, 1)
			l_height := layout_height(l_surface, a_text).max (height)
			if
				attached {GAME_TEXTURE_CAIRO} texture as la_texture and
				l_height = texture.height and l_width = texture.width
			then
				l_texture := la_texture
			else
				if l_height > renderer.driver.maximum_texture_height then
					l_height :=renderer.driver.maximum_texture_height
				end
				if l_width > renderer.driver.maximum_texture_width then
					l_width :=renderer.driver.maximum_texture_width
				end
				if l_height < height then
					l_height := height
				end
				create l_texture.make (renderer, pixel_format, l_width, l_height)
			end
			texture := l_texture
			l_texture.lock
			l_surface := l_texture.cairo_surface
			draw_text(l_surface, a_text)
			l_surface.show_page
			l_texture.unlock
			scroll_text (0)
		end

	width:INTEGER assign set_width
			-- <Precursor>

	set_width(a_width:INTEGER)
			-- Assign `width' with the value of `a_width'
		require
			Width_Positive: a_width > 0
		do
			width := a_width
		ensure
			Is_Assign: width ~ a_width
		end


	height:INTEGER assign set_height
			-- <Precursor>

	set_height(a_height:INTEGER)
			-- Assign `height' with the value of `a_height'
		require
			Height_Positive: a_height > 0
		do
			height := a_height
		ensure
			Is_Assign: height ~ a_height
		end

	is_alignment_left:BOOLEAN
			-- `Text' is align at the left

	set_alignment_left
			-- Align `Text' at the left
		do
			disable_alignment
			is_alignment_left := True
		end

	is_alignment_center:BOOLEAN
			-- `Text' is centered

	set_alignment_center
			-- Center `Text'
		do
			disable_alignment
			is_alignment_center := True
		end

	is_alignment_right:BOOLEAN
			-- `Text' is align at the right

	set_alignment_right
			-- Align `Text' at the right
		do
			disable_alignment
			is_alignment_right := True
		end

	is_alignment_justified:BOOLEAN
			-- `Text' is justified

	set_alignment_justified
			-- Justified `Text'
		do
			disable_alignment
			is_alignment_justified := True
		end

	font_description:PANGO_FONT_DESCRIPTION
			-- The description of the font to use in Pango

	start_y:INTEGER
			-- The vertical position to start to show `text_texture'

	scroll_text(a_delta:INTEGER)
			-- Scroll the `text_texture' of `a_delta' pixel
		do
			start_y := start_y + a_delta
			if start_y + height > texture.height then
				start_y := texture.height - height
			end
			if start_y  < 0 then
				start_y := 0
			end
		end

	change_weight
			-- Circularly change the weight (bold) of the text of `Current'
		local
			l_new_weight:INTEGER
		do
			l_new_weight := font_description.weight + 400
			if l_new_weight > 1000 then
				l_new_weight := 100
			end
			font_description.set_weight (l_new_weight)
		end

	toggle_italic
			-- Set or unset the italic font
		do
			if font_description.is_style_italic then
				font_description.set_style_normal
			else
				font_description.set_style_italic
			end
		end

feature {NONE} -- Implementation

	disable_alignment
			-- Remove every alignment
		do
			is_alignment_left := False
			is_alignment_center := False
			is_alignment_right := False
			is_alignment_justified := False
		end

	layout_height(a_cairo_surface:CAIRO_SURFACE_IMAGE; a_text:READABLE_STRING_GENERAL):INTEGER
			-- The height of the `Text' when drawn on `a_cairo_surface'
		require
			Cairo_Surface_Valid: a_cairo_surface.is_valid
		local
			l_context:CAIRO_CONTEXT
			l_layout:PANGO_CAIRO_LAYOUT
		do
			create l_context.make (a_cairo_surface)
			l_context.move_to (0, 0)
			create l_layout.make (l_context)
			l_layout.set_width (a_cairo_surface.width * Pango_scale)
			l_layout.set_font_description (font_description)
			l_layout.set_text (a_text)
			Result := l_layout.logical_extents_pixels.height
		end


	draw_text(a_cairo_surface:CAIRO_SURFACE_IMAGE; a_text:READABLE_STRING_GENERAL)
			-- Draw `text' on `a_cairo_surface'
		require
			Cairo_Surface_Valid: a_cairo_surface.is_valid
		local
			l_context:CAIRO_CONTEXT
			l_layout:PANGO_CAIRO_LAYOUT
		do
			create l_context.make (a_cairo_surface)
			l_context.set_source_rgb (1.0, 1.0, 1.0)
			l_context.paint
			l_context.set_source_rgb (0.0, 0.0, 0.0)
			l_context.move_to (0, 0)
			create l_layout.make (l_context)
			l_layout.set_width (a_cairo_surface.width * Pango_scale)
			l_layout.set_font_description (font_description)
			l_layout.set_text (a_text)
			if is_alignment_left then
				l_layout.set_alignment_left
			elseif is_alignment_center then
				l_layout.set_alignment_center
			elseif is_alignment_right then
				l_layout.set_alignment_right
			elseif is_alignment_justified then
				l_layout.enable_justify
			end
			l_layout.update_cairo_context
			l_layout.show
		end

	pixel_format:GAME_PIXEL_FORMAT
			-- {GAME_PIXEL_FORMAT} used to create `text_texture'
		once
			create Result
			Result.set_argb8888
		end

feature {NONE} -- Constants

	font_description_text:STRING = "sans 14"
			-- A text description of the `font_description'

invariant


note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
