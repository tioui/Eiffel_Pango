note
	description: "An example of using markup language."
	author: "Louis Marchand"
	date: "Sat, 30 Jun 2018 13:38:36 +0000"
	revision: "0.1"

class
	APPLICATION

create
	make

feature {NONE} -- Constants

	Font_description_text:STRING = "arial 35"
			-- The font to use (unless specified in the markup `Text'

	Text:STRING_32 = "[
<span foreground="blue" font_family="Station">
   <b> Hello </b>
   <u> Eiffel </u>
   <i> World </i>
</span>
<tt> Hello </tt>
<span font_family="sans" font_stretch="ultracondensed" letter_spacing="500" font_weight="light"> EIFFEL</span>
<span foreground="#FFCC00"> world</span>
	]"
			-- The text with markup.
			-- For language details, see: https://developer.gnome.org/pango/stable/PangoMarkupFormat.html

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_surface:CAIRO_SURFACE_IMAGE
			l_cairo_context:CAIRO_CONTEXT
		do
			create l_surface.make (create {CAIRO_PIXEL_FORMAT}.make_argb32, 500, 500)
			if l_surface.is_valid then
				create l_cairo_context.make (l_surface)
				if l_cairo_context.is_valid then
					l_cairo_context.set_source_rgb (1.0, 1.0, 1.0)
					l_cairo_context.paint
					show_text(l_cairo_context)
					l_surface.show_page
					l_surface.save_to_png ("test.png")
				else
					io.error.put_string ("Error: could not create Cairo Context.")
				end
			else
				io.error.put_string ("Error: could not create Cairo Surface.")
			end
		end

feature -- Access

	show_text(a_cairo_context:CAIRO_CONTEXT)
			-- Draw the `text' on `a_cairo_context' using markup language
		require
			Cairo_Context_Is_Valid: a_cairo_context.is_valid
		local
			l_pango_layout:PANGO_CAIRO_LAYOUT
			l_font_description:PANGO_FONT_DESCRIPTION
		do
			create l_pango_layout.make (a_cairo_context)
			create l_font_description.make_from_text (Font_description_text)
			l_pango_layout.set_font_description (l_font_description)
			a_cairo_context.move_to (10, 10)
			a_cairo_context.set_source_rgb (0.0, 0.0, 0.0)
			l_pango_layout.set_text_with_markup (Text)
			l_pango_layout.update_cairo_context
			l_pango_layout.show
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"
end
