note
	description: "[
			Exemple of using basic text drawing with Pango and CairoBase on this example:
			https://developer.gnome.org/pango/stable/pango-Cairo-Rendering.html#pango-Cairo-Rendering.description
		]"
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

class
	APPLICATION

inherit
	DOUBLE_MATH
	PANGO_CONSTANTS

create
	make

feature {NONE} -- Constants

	Words_Count:INTEGER = 10
			-- The number of words to draw

	Radius:INTEGER = 200
			-- The radius of the words circle

	Text:STRING = "Eiffel"
			-- The text to show

	Font:STRING = "Sans Bold 27"
			-- The font description to use

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_surface:CAIRO_SURFACE_IMAGE
			l_cairo_context:CAIRO_CONTEXT
		do
			create l_surface.make (create {CAIRO_PIXEL_FORMAT}.make_argb32, Radius * 2, Radius * 2)
			create l_cairo_context.make (l_surface)
			l_cairo_context.set_source_rgb (1, 1, 1)
			l_cairo_context.paint
			l_cairo_context.translate (Radius, Radius)
			draw_circle(l_cairo_context)
			l_surface.save_to_png ("test.png")
		end

	draw_circle(a_cairo_context:CAIRO_CONTEXT)
			-- Draw a circle of `Text' on `a_cairo_context'
		require
			Cairo_Context_Valid:a_cairo_context.is_valid
		local
			l_layout:PANGO_CAIRO_LAYOUT
			l_font_description:PANGO_FONT_DESCRIPTION
			l_index:INTEGER
			l_size:TUPLE[width, height:INTEGER]
			l_angle, l_red:REAL_64
		do
			create l_layout.make (a_cairo_context)
			l_layout.set_text (Text)
			create l_font_description.make_from_text (Font)
			l_layout.set_font_description (l_font_description)
			from
				l_index := 0
			until
				l_index >= 10
			loop
				l_angle := (360.0 * l_index) / Words_Count
				a_cairo_context.save_state
				l_red := (1.0 + cosine ((l_angle - 60.0) * Pi / 180.0)) / 2.0
				a_cairo_context.set_source_rgb (l_red, 0.0, 1.0 - l_red)
				a_cairo_context.rotate (l_angle * Pi / 180.0)
				l_size := l_layout.size
				a_cairo_context.move_to (-(l_size.width / pango_scale) / 2.0, -Radius)
				l_layout.update_cairo_context
				l_layout.show
				a_cairo_context.restore_state
				l_index := l_index + 1
			end
			a_cairo_context.show_page
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
