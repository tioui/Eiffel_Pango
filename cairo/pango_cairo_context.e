note
	description: "A {PANGO_CONTEXT} to use with Cairo."
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

class
	PANGO_CAIRO_CONTEXT

inherit
	PANGO_CONTEXT
	PANGO_CAIRO_CONTEXT_CONTAINER
		rename
			valid as exists
		redefine
			make, set_cairo_context
		end
	CAIRO_ANY

create
	make

feature {NONE} -- Initialization

	make(a_cairo_context:CAIRO_CONTEXT)
			-- <Precursor>
		do
			Precursor(a_cairo_context)
			make_owned ({PANGO_CAIRO_EXTERNAL}.pango_cairo_create_context(a_cairo_context.item))
		end

feature -- Access

	set_cairo_context(a_cairo_context:CAIRO_CONTEXT)
			-- <Precursor>
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_update_context(a_cairo_context.item, item)
			Precursor(a_cairo_context)
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
