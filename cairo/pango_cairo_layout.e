note
	description: "A {PANGO_LAYOUT} to use with Cairo."
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

class
	PANGO_CAIRO_LAYOUT

inherit
	PANGO_LAYOUT
		rename
			make as make_from_pango_cairo_context
		redefine
			make_from_pango_cairo_context
		end
	PANGO_CAIRO_CONTEXT_CONTAINER
		rename
			valid as exists
		undefine
			copy, set_cairo_context
		redefine
			make
		end
	CAIRO_ANY
		undefine
			copy
		end

create
	make

feature {NONE} -- Initialization

	make(a_cairo_context:CAIRO_CONTEXT)
			-- <Precursor>
		do
			Precursor(a_cairo_context)
			make_owned ({PANGO_CAIRO_EXTERNAL}.pango_cairo_create_layout(a_cairo_context.item))
		end

	make_from_pango_cairo_context(a_pango_cairo_context:PANGO_CAIRO_CONTEXT)
			-- Initialization of `Current' using `a_pango_cairo_context' as `context'
		do
			cairo_context := a_pango_cairo_context.cairo_context
			Precursor(a_pango_cairo_context)
		end

feature -- Access

	set_cairo_context(a_cairo_context:CAIRO_CONTEXT)
			-- <Precusror>
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_update_layout(a_cairo_context.item, item)
			Precursor(a_cairo_context)
		end

	show
			-- Draw `Current' in `cairo_context'
			-- The origin of the glyphs (the left edge of the line) will be drawn
			-- at the current point of the `cairo_context'.
		require
			Exists: exists
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_show_layout(cairo_context.item, item)
		end

	show_line(a_line:PANGO_LINE)
			-- Draw `a_line' in `cairo_context'
			-- The origin of the glyphs (the left edge of the line) will be drawn
			-- at the current point of the `cairo_context'.
		require
			Exists: exists
			Line_Exists: a_line.exists
			Line_Layout_Valid: a_line.layout ~ Current
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_show_layout_line(cairo_context.item, a_line.item)
		end

	path
			-- Add `Current'to the path of `cairo_context'
		require
			Exists: exists
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_layout_path(cairo_context.item, item)
		end

	path_line(a_line:PANGO_LINE)
			-- Add `a_line'to the path of `cairo_context'
		require
			Exists: exists
			Line_Exists: a_line.exists
			Line_Layout_Valid: a_line.layout ~ Current
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_layout_line_path(cairo_context.item, a_line.item)
		end

	show_error_underline(a_x, a_y, a_width, a_height:REAL_64)
			-- Draw a squiggly line in the `cairo_context' that approximately
			-- covers the given rectangle in the style of an underline used to indicate a spelling error.
		require
			Width_Positive: a_width > 0
			Width_Positive: a_height > 0
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_show_error_underline(cairo_context.item, a_x, a_y, a_width, a_height)
		end

	path_error_underline(a_x, a_y, a_width, a_height:REAL_64)
			-- Add a squiggly line to the path of `cairo_context' that approximately
			-- covers the given rectangle in the style of an underline used to indicate a spelling error.
		require
			Width_Positive: a_width > 0
			Width_Positive: a_height > 0
		do
			{PANGO_CAIRO_EXTERNAL}.pango_cairo_show_error_underline(cairo_context.item, a_x, a_y, a_width, a_height)
		end

invariant
	Cairo_Context_Valid: cairo_context.is_valid


note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
