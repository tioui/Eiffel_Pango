note
	description: "Common ancestor of Eiffel_Pango classes containing {CAIRO_CONTEXT}"
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

deferred class
	PANGO_CAIRO_CONTEXT_CONTAINER

inherit
	PANGO_ANY

feature {NONE} -- Initialization

	make(a_cairo_context:CAIRO_CONTEXT)
			-- Initialization of `Current' using `a_cairo_context' as `cairo_context'
		require
			Cairo_Context_Valid: a_cairo_context.is_valid
		do
			cairo_context := a_cairo_context
		end

feature -- Access

	valid:BOOLEAN
			-- `Current' is valid
		deferred
		end

	cairo_context:CAIRO_CONTEXT assign set_cairo_context
			-- The {CAIRO_CONTEXT} associate with `Current'

	set_cairo_context(a_cairo_context:CAIRO_CONTEXT)
			-- Assign `cairo_context' with the value of `a_cairo_context'
			-- The transformation and target surface of a `cairo_context' will be updated
			-- (no need to use `update_cairo_context' after this setter)
		require
			Is_Valid: valid
			cairo_context.is_valid
		do
			cairo_context := a_cairo_context
		ensure
			Is_Assign:cairo_context ~ a_cairo_context
		end

	update_cairo_context
			-- Update `Current' using the value from `a_cairo_context'
			-- If the initial {CAIRO_CONTEXT} is modified, this method must be called
		require
			Is_Valid: valid
		do
			set_cairo_context(cairo_context)
		end

invariant
	Cairo_Context_Valid: cairo_context.is_valid


note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"


end
