note
	description: "A {LIST} that contain {PANGO_ITEM}"
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

class
	PANGO_ITEM_LIST

inherit
	DYNAMIC_LIST [PANGO_ITEM]
		redefine
			default_create, copy, extendible, prunable, replaceable, writable, readable
		end
	PANGO_ANY
		undefine
			is_equal
		redefine
			default_create, copy
		end
	DISPOSABLE
		undefine
			is_equal
		redefine
			default_create, copy
		end


create {PANGO_ANY}
	from_glist

create
	default_create

feature -- Initializtion

	from_glist(a_internal_item:POINTER)
			-- Initialization of `Current' using
			-- `a_internal_item' as internal list
		do
			internal_item := a_internal_item
			index := 1
		end

	default_create
			-- Initialization of `Current'
		do
			from_glist(create {POINTER})
		end

feature -- Access

	cursor: PANGO_LIST_CURSOR
			-- <Precursor>
		do
			create Result.make(index)
		end

	index: INTEGER_32
			-- <Precursor>

	item:PANGO_ITEM
			-- <Precursor>
		do
			create Result.make_owned ({PANGO_EXTERNAL}.pango_item_copy ({PANGO_EXTERNAL}.g_list_nth_data(internal_item, (index - 1).to_natural_32)))
		end

feature -- Element change

	extend (a_value: like item)
			-- Add a new occurrence of `a_value'.
		do
			internal_item := {PANGO_EXTERNAL}.g_list_append(internal_item, {PANGO_EXTERNAL}.pango_item_copy (a_value.item))
		end

	put_right (a_value: like item)
			-- Add `a_value' to the right of cursor position.
			-- Do not move cursor.
		do
			if index = count then
				extend(a_value)
			else
				internal_item := {PANGO_EXTERNAL}.g_list_insert(internal_item, {PANGO_EXTERNAL}.pango_item_copy (a_value.item), index)
			end
		end

	put_front (a_value: like item)
			-- Add `a_value` at beginning.
			-- Do not move cursor.
		do
			internal_item := {PANGO_EXTERNAL}.g_list_prepend(internal_item, {PANGO_EXTERNAL}.pango_item_copy (a_value.item))
			if not before then
				index := index + 1
			end
		end

feature -- Element change

	replace (a_value: like item)
			-- Replace current item by `a_value'.
		local
			l_element, l_data:POINTER
		do
			l_element := {PANGO_EXTERNAL}.g_list_nth(internal_item, (index - 1).to_natural_32)
			l_data := {PANGO_EXTERNAL}.glist_struct_get_data(l_element)
			{PANGO_EXTERNAL}.glist_struct_set_data(l_element, {PANGO_EXTERNAL}.pango_item_copy (a_value.item))
			{PANGO_EXTERNAL}.pango_item_free(l_data)
		end

feature -- Measurement

	count:INTEGER
			-- <Precursor>
		do
			if exists then
				Result := {PANGO_EXTERNAL}.g_list_length(internal_item).to_integer_32
			else
				Result := 0
			end
		end

feature -- Cursor movement

	back
			-- <Precursor>
		do
			index := index - 1
		end

	forth
			-- <Precursor>
		do
			index := index + 1
		end

	go_to (p: CURSOR)
			-- <Precursor>
		do
			if attached {PANGO_LIST_CURSOR} p as al_c then
				index := al_c.index
			else
				check
					correct_cursor_type: False
				end
			end
		end

feature -- Status report

	exists:BOOLEAN
			-- `Current' has a memory value
		do
			Result := not internal_item.is_default_pointer
		end

	readable: BOOLEAN
			-- <Precursor>
		do
			Result := exists and Precursor
		end

	writable: BOOLEAN
			-- <Precursor>
		do
			Result := exists and Precursor
		end

	extendible: BOOLEAN
			-- May new items be added?
		do
			Result := exists
		end

	prunable: BOOLEAN
			-- May items be removed?
		do
			Result := exists
		end

	replaceable: BOOLEAN
			-- <Precursor>
		do
			Result := exists
		end

	full: BOOLEAN = False
			-- <Precursor>

	valid_cursor (p: CURSOR): BOOLEAN
			-- <Precursor>
		do
			if attached {PANGO_LIST_CURSOR} p as al_c then
				Result := valid_cursor_index (al_c.index)
			end
		end

feature -- Removal

	remove
			-- <Precursor>
		local
			l_data:POINTER
		do
			l_data := {PANGO_EXTERNAL}.g_list_nth_data(internal_item, (index - 1).to_natural_32)
			internal_item := {PANGO_EXTERNAL}.g_list_delete_link(internal_item, {PANGO_EXTERNAL}.g_list_nth(internal_item, (index - 1).to_natural_32))
			{PANGO_EXTERNAL}.pango_item_free(l_data)
		end

	remove_left
			-- <Precursor>
		do
			index := index - 1
			remove
		end

	remove_right
			-- <Precursor>
		do
			index := index + 1
			remove
			index := index - 1
		end

feature -- Duplication

	copy (a_other: attached PANGO_ITEM_LIST)
			-- <Precursor>
		do
			dispose
			if a_other.exists then
				internal_item := {PANGO_EXTERNAL}.g_list_copy(a_other.internal_item)
			else
				internal_item := a_other.internal_item
			end

		end

	reorder:PANGO_ITEM_LIST
			-- Produce a list in visual order.
		do
			create Result.from_glist ({PANGO_EXTERNAL}.pango_reorder_items(internal_item))
		end

feature {PANGO_ITEM_LIST} -- Implementation

	new_chain: like Current
			-- <Precursor>
		do
			create Result
		end

feature {PANGO_ANY} -- Implementation

	internal_item:POINTER
			-- Internal representation of `Current'

feature {NONE} -- Removal

	dispose
			-- <Precursor>
		local
			l_next:POINTER
		do
			if exists then
				from
				l_next := internal_item
				until
					l_next.is_default_pointer
				loop
					{PANGO_EXTERNAL}.pango_item_free({PANGO_EXTERNAL}.glist_struct_get_data(l_next))
					l_next := {PANGO_EXTERNAL}.glist_struct_get_next(l_next)
				end
				{PANGO_EXTERNAL}.g_list_free(internal_item)
			end

		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
