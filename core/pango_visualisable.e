note
	description: "A common ancestors for clases with extents"
	author: "Louis Marchand"
	date: "Sat, 30 Jun 2018 13:38:36 +0000"
	revision: "0.1"

deferred class
	PANGO_VISUALISABLE

inherit
	PANGO_ANY

feature -- Access

	valid:BOOLEAN
			-- `Current' is valid
		deferred
		end

	logical_extents:TUPLE[x, y, width, height:INTEGER]
			-- The logical extents of `Current' in Pango units.
			-- Logical extents are usually what you want for positioning things
			-- Note that both extents may have non-zero x and y.
			-- You may want to use those to offset where you render the layout
		require
			Valid: valid
		do
			Result := launch_for_rectangle(logical_extents_routine)
		end

	ink_extents:TUPLE[x, y, width, height:INTEGER]
			-- The extents of `Current' as drawn in Pango units.
			-- Note that both extents may have non-zero x and y.
			-- You may want to use those to offset where you render the layout
		require
			Valid: valid
		do
			Result := launch_for_rectangle(ink_extents_routine)
		end

	logical_extents_pixels:TUPLE[x, y, width, height:INTEGER]
			-- The logical extents of `Current' in device units.
			-- Note that both extents may have non-zero x and y.
			-- You may want to use those to offset where you render the layout
		require
			Valid: valid
		do
			Result := launch_for_rectangle(logical_extents_pixels_routine)
		end

	ink_extents_pixels:TUPLE[x, y, width, height:INTEGER]
			-- The extents of `Current' as drawn in device units.
			-- Note that both extents may have non-zero x and y.
			-- You may want to use those to offset where you render the layout
		require
			Valid: valid
		do
			Result := launch_for_rectangle(ink_extents_pixels_routine)
		end

feature {PANGO_ANY} -- Implementation

	item:POINTER
			-- Internal representation of `Current'
		deferred
		end

feature {NONE} -- Implementation

	null:POINTER
			-- A  NULL C pointer
		once
			create Result
		end

	logical_extents_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		deferred
		end

	ink_extents_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		deferred
		end

	logical_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		deferred
		end

	ink_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		deferred
		end


	rectangle_information(a_rectangle:POINTER):TUPLE[x, y, width, height:INTEGER]
			-- Get the position and dimension of an internal PangoRecangle
			-- pointed by `a_rectangle'
		local
			l_x, l_y, l_width, l_height:INTEGER
		do
			l_x := {PANGO_EXTERNAL}.PangoRectangle_x_get(a_rectangle)
			l_y := {PANGO_EXTERNAL}.PangoRectangle_y_get(a_rectangle)
			l_width := {PANGO_EXTERNAL}.PangoRectangle_width_get(a_rectangle)
			l_height := {PANGO_EXTERNAL}.PangoRectangle_height_get(a_rectangle)
			Result := [l_x, l_y, l_width, l_height]
		end

	launch_for_rectangle(a_routine:PROCEDURE[POINTER]):TUPLE[x, y, width, height:INTEGER]
			-- Launch `a_routine' that need a PangoRectangle {POINTER} and return
			-- the dimension and position of this PangoRectangle after the launch
		local
			l_rect:POINTER
		do
			l_rect := {PANGO_EXTERNAL}.new_PangoRectangle
			a_routine(l_rect)
			Result := rectangle_information(l_rect)
			{PANGO_EXTERNAL}.delete_PangoRectangle(l_rect)
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
