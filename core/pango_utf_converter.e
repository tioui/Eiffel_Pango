note
	description: "Specialized version of an {UTF_CONVERTER}."
	author: "Louis Marchand"
	date: "Sat, 30 Jun 2018 13:38:36 +0000"
	revision: "0.1"

expanded class
	PANGO_UTF_CONVERTER

inherit
	UTF_CONVERTER
		redefine
			utf_8_bytes_count
		end

feature -- Measurement

	utf_8_bytes_count (a_string: READABLE_STRING_GENERAL; a_start_pos, a_end_pos: INTEGER): INTEGER
			-- Number of bytes necessary to encode in UTF-8 `a_string.substring (a_start_pos, a_end_pos)'.
			-- Note that this feature can be used for both escaped and non-escaped string.
			-- In the case of escaped strings, the result will be possibly higher than really needed.
			-- It does not include the terminating null character.
		require else
			start_position_big_enough: a_start_pos >= 1
			end_position_big_enough: a_start_pos <= a_end_pos
			end_pos_small_enough: a_end_pos <= a_string.count
		local
			i: INTEGER
			c: NATURAL_32
		do
			from
				i := a_start_pos
			until
				i > a_end_pos
			loop
				c := a_string.code (i)
				if c <= 0x7F then
						-- 0xxxxxxx.
					Result := Result + 1
				elseif c <= 0x7FF then
						-- 110xxxxx 10xxxxxx
					Result := Result + 2
				elseif c <= 0xFFFF then
						-- 1110xxxx 10xxxxxx 10xxxxxx
					Result := Result + 3
				else
						-- c <= 1FFFFF - there are no higher code points
						-- 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
					Result := Result + 4
				end
				i := i + 1
			end
		end

	utf_8_string_8_to_string_32_count (a_string: READABLE_STRING_8; a_start_pos, a_end_pos: INTEGER): INTEGER
			-- Count of characters corresponding to UTF-8 sequence `a_string', starting at postition `a_start_pos'
			-- and stopping at position `a_end_pos'.
		require
			start_position_big_enough: a_start_pos > 0
			end_position_big_enough: a_start_pos <= a_end_pos
			end_pos_small_enough: a_end_pos <= a_string.count
		local
			i: INTEGER
			n: INTEGER
			c: INTEGER
		do
			from
				i := a_start_pos
				n := a_end_pos
			until
				i > n
			loop
				c := a_string [i].code
				if c <= 0x7F then
						-- 0xxxxxxx
					i := i + 1
					Result := Result + 1
				elseif c <= 0xDF then
						-- 110xxxxx 10xxxxxx
					i := i + 2
					if i <= n then
						Result := Result + 1
					end
				elseif c <= 0xEF then
						-- 1110xxxx 10xxxxxx 10xxxxxx
					i := i + 3
					if i <= n then
						Result := Result + 1
					end
				elseif c <= 0xF7 then
						-- 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
					i := i + 4
					if i <= n then
						Result := Result + 1
					end
				end
			end
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
