note
	description: "Used to fetch and query system fonts"
	author: "Louis Marchand"
	date: "Thu, 28 Jun 2018 16:18:54 +0000"
	revision: "0.1"

class
	PANGO_FONT_DESCRIPTION

inherit
	PANGO_FONT_DESCRIPTION_READABLE
		redefine
			default_create
		end


create
	default_create,
	make_from_text

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			make_owned({PANGO_EXTERNAL}.pango_font_description_new)
		end

	make_from_text(a_text:READABLE_STRING_GENERAL)
			--Initialization of `Current' using `a_text' as `text'
			-- The `a_text' must be in the form "FAMILY-LIST [SIZE]",
			-- where FAMILY-LIST is a comma separated list of families
			-- optionally terminated by a comma, STYLE_OPTIONS is a
			-- whitespace separated list of words where each word describes
			-- one of style, variant, weight, stretch, or gravity, and SIZE
			-- is a decimal number (size in points) or optionally followed
			-- by the unit modifier "px" for absolute size. Any one of the
			-- options may be absent.
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_utf
			create l_c_string.make (l_utf.string_32_to_utf_8_string_8 (a_text.to_string_32))
			make_owned({PANGO_EXTERNAL}.pango_font_description_from_string(l_c_string.item))
		end

feature -- Access

	set_family(a_name:READABLE_STRING_GENERAL)
			-- Assign `family' with the value of `a_name'
		require
			Exists: exists
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_utf
			create l_c_string.make (l_utf.string_32_to_utf_8_string_8 (a_name.to_string_32))
			{PANGO_EXTERNAL}.pango_font_description_set_family(item, l_c_string.item)
		ensure
			Is_Assign: family ~ a_name
		end

	set_style_normal
			-- Present `Current' using normal font style
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_style(item, {PANGO_EXTERNAL}.pango_style_normal)
		ensure
			Is_Set: is_style_normal
		end

	set_style_italic
			-- Present `Current' using italic font style
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_style(item, {PANGO_EXTERNAL}.pango_style_italic)
		ensure
			Is_Set: is_style_italic
		end

	set_style_oblique
			-- Present `Current' using oblique font style
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_style(item, {PANGO_EXTERNAL}.pango_style_oblique)
		ensure
			Is_Set: is_style_oblique
		end

	enable_small_caps
			-- `Current' with the lower case characters replaced by smaller variants of the capital characters.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_variant(item, {PANGO_EXTERNAL}.pango_variant_small_caps)
		end

	disable_small_caps
			-- `Current' with the lower case characters not replaced by smaller variants of the capital characters.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_variant(item, {PANGO_EXTERNAL}.pango_variant_normal)
		end

	set_weight(a_weight:INTEGER)
			-- Assign `weight' with the value of `a_weight'
		require
			Exists: exists
			Weight_Valid: a_weight >= 100 and a_weight <= 1000
		do
			{PANGO_EXTERNAL}.pango_font_description_set_weight(item, a_weight)
		ensure
			Is_Assign: weight ~ a_weight
			Weight_Valid: weight >= 100 and weight <= 1000
		end

	set_stretch_ultra_condensed
			-- Set `Current' with an ultra condensed streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_ultra_condensed)
		ensure
			Is_Set: is_stretch_ultra_condensed
		end

	set_stretch_extra_condensed
			-- Set `Current' with an extra condensed streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_extra_condensed)
		ensure
			Is_Set: is_stretch_extra_condensed
		end

	set_stretch_condensed
			-- Set `Current' with a condensed streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_condensed)
		ensure
			Is_Set: is_stretch_condensed
		end

	set_stretch_semi_condensed
			-- Set `Current' with a semi condensed streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_semi_condensed)
		ensure
			Is_Set: is_stretch_semi_condensed
		end

	set_stretch_normal
			-- Set `Current' with no streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_normal)
		ensure
			Is_Set: is_stretch_normal
		end

	set_stretch_semi_expanded
			-- Set `Current' with a semi expanded streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_semi_expanded)
		ensure
			Is_Set: is_stretch_semi_expanded
		end

	set_stretch_expanded
			-- Set `Current' with an expanded streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_expanded)
		ensure
			Is_Set: is_stretch_expanded
		end

	set_stretch_extra_expanded
			-- Set `Current' with an extra expanded streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_extra_expanded)
		ensure
			Is_Set: is_stretch_extra_expanded
		end

	set_stretch_ultra_expanded
			-- Set `Current' with an ultra expanded streching
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_stretch(item, {PANGO_EXTERNAL}.pango_stretch_ultra_expanded)
		ensure
			Is_Set: is_stretch_ultra_expanded
		end

	set_size(a_size:INTEGER)
			-- Assign `size' with the value of `a_size' in point
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_size(item, a_size)
		ensure
			Is_Assign: size ~ a_size
			Is_Not_Absolute: not is_absolute_size
		end

	set_absolute_size(a_size:INTEGER)
			-- Assign `size' with the value of `a_size' in device unit
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_absolute_size(item, a_size)
		ensure
			Is_Assign: size ~ a_size
			Is_Absolute: is_absolute_size
		end

	set_gravity_auto
			-- The gravity is manage by the {PANGO_CONTEXT} and not by `Current'
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_gravity(item, {PANGO_EXTERNAL}.pango_gravity_auto)
		ensure
			Is_Set: is_gravity_auto
		end

	set_gravity_south
			-- Glyphs stand upright (default)
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_gravity(item, {PANGO_EXTERNAL}.pango_gravity_south)
		ensure
			Is_Set: is_gravity_south
		end

	set_gravity_east
			-- Glyphs are rotated 90 degrees clockwise
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_gravity(item, {PANGO_EXTERNAL}.pango_gravity_east)
		ensure
			Is_Set: is_gravity_east
		end

	set_gravity_north
			-- Glyphs are upside-down
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_gravity(item, {PANGO_EXTERNAL}.pango_gravity_north)
		ensure
			Is_Set: is_gravity_north
		end

	set_gravity_west
			-- Glyphs are rotated 90 degrees counter-clockwise
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_font_description_set_gravity(item, {PANGO_EXTERNAL}.pango_gravity_west)
		ensure
			Is_Set: is_gravity_west
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
