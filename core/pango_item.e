note
	description: "Segment that contain pieces of texts."
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"
	todo: "PangoAnalysis"

class
	PANGO_ITEM

inherit
	PANGO_MEMORY_STRUCTURE
		redefine
			default_create, dispose, copy, is_equal
		end

create {PANGO_ANY}
	make_owned,
	make_shared

create
	default_create

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			make_owned({PANGO_EXTERNAL}.pango_item_new)
		end

feature -- Access

	offset:INTEGER
			-- byte offset of the start of `Current' in text
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pangoitem_struct_get_offset(item)
		end

	length:INTEGER
			-- syze in byte of `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pangoitem_struct_get_length(item)
		end

	character_count:INTEGER
			-- number of Unicode character in `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pangoitem_struct_get_length(item)
		end

	split(a_length, a_offset:INTEGER):TUPLE[before, after:PANGO_ITEM]
			-- Split `Current' in two. The `before' will be of size `length'.
			-- The `a_offset' is the number of character before `a_length' in `Current'.
			-- `after' will have the rest.
		require
			Exists: exists
			Lenght_Strictly_Positive: a_length > 0
			Offset_Strictly_Positive: a_offset > 0
			Lenght_Valid: a_length < length
			Offset_Strictly_Positive: a_offset < character_count
		local
			l_before, l_after:PANGO_ITEM
		do
			l_after := twin
			create l_before.make_owned ({PANGO_EXTERNAL}.pango_item_split(l_after.item, a_length, a_offset))
			Result := [l_before, l_after]
		ensure
			Length_Valid: Result.before.length ~ a_length and Result.after.length ~ length - a_length
		end


feature -- Duplication

	copy (a_other: like Current)
			-- <Precursor>
		do
			make_owned({PANGO_EXTERNAL}.pango_item_copy(a_other.item))
		end

feature -- Comparison

	is_equal (a_other: like Current): BOOLEAN
			-- <Precursor>
		do
			Result :=
					offset ~ a_other.offset and
					length ~ a_other.length and
					character_count ~ a_other.character_count
		end

feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists and not shared then
				{PANGO_EXTERNAL}.pango_item_free (item)
			end
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
