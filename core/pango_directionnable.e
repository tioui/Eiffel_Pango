note
	description: "Common class of classes that can verify the direction of text."
	author: "Louis Marchand"
	date: "Sat, 30 Jun 2018 13:38:36 +0000"
	revision: "0.1"

deferred class
	PANGO_DIRECTIONNABLE

inherit
	PANGO_ANY

feature -- Status report

	valid:BOOLEAN
			-- `Current' is valid
		deferred
		end

	is_direction_left_to_right:BOOLEAN
			-- `True' if `Current' use a strong left-to-right direction
		require
			Is_Valid: valid
		do
			Result := direction_index = {PANGO_EXTERNAL}.pango_direction_ltr
		end

	is_direction_right_to_left:BOOLEAN
			-- `True' if `Current' use a strong right-to-left direction
		require
			Is_Valid: valid
		do
			Result := direction_index = {PANGO_EXTERNAL}.pango_direction_rtl
		end

	is_direction_weak_left_to_right:BOOLEAN
			-- `True' if `Current' use a weak left-to-right direction
		require
			Is_Valid: valid
		do
			Result := direction_index = {PANGO_EXTERNAL}.pango_direction_weak_ltr
		end

	is_direction_weak_right_to_left:BOOLEAN
			-- `True' if `Current' use a weak right-to-left direction
		require
			Is_Valid: valid
		do
			Result := direction_index = {PANGO_EXTERNAL}.pango_direction_weak_rtl
		end

	is_direction_neutral:BOOLEAN
			-- `True' if `Current' do not specifie a direction
		require
			Is_Valid: valid
		do
			Result := direction_index = {PANGO_EXTERNAL}.pango_direction_neutral
		end


feature {NONE} -- Implementation

	direction_index:INTEGER
			-- The PangoDirection internal index
		deferred
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
