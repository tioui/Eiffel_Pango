note
	description: "A readable version of {PANGO_FONT_DESCRIPTION}"
	author: "Louis Marchand"
	date: "Thu, 28 Jun 2018 16:18:54 +0000"
	revision: "0.1"

class
	PANGO_FONT_DESCRIPTION_READABLE

inherit
	HASHABLE
		redefine
			copy, is_equal
		end
	PANGO_MEMORY_STRUCTURE
		redefine
			dispose, copy, is_equal
		end

create {PANGO_ANY}
	make_owned,
	make_shared

feature -- Access

	text:READABLE_STRING_GENERAL
			-- A text representation of `Current'
		require
			Exists: exists
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_utf
			create l_c_string.own_from_pointer ({PANGO_EXTERNAL}.pango_font_description_to_string(item))
			Result := l_utf.utf_8_string_8_to_string_32 (l_c_string.string)
		end

	hash_code: INTEGER_32
			-- Hash code value
		do
			if exists then
				Result := {PANGO_EXTERNAL}.pango_font_description_hash(item).hash_code
			else
				Result := 0
			end

		end

	family:READABLE_STRING_GENERAL
			-- The font family of `Current'
		require
			Exists: exists
		local
			l_pointer:POINTER
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_utf
			l_pointer := {PANGO_EXTERNAL}.pango_font_description_get_family(item)
			if l_pointer.is_default_pointer then
				Result := ""
			else
				create l_c_string.make_shared_from_pointer (l_pointer)
				Result := l_utf.utf_8_string_8_to_string_32 (l_c_string.string)
			end
		end


	is_style_normal:BOOLEAN
			-- `True' if `Current' is presented using normal font style
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_style(item) = {PANGO_EXTERNAL}.pango_style_normal
		end

	is_style_italic:BOOLEAN
			-- `True' if `Current' is presented using italic font style
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_style(item) = {PANGO_EXTERNAL}.pango_style_italic
		end

	is_style_oblique:BOOLEAN
			-- `True' if `Current' is presented using oblique font style
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_style(item) = {PANGO_EXTERNAL}.pango_style_oblique
		end

	is_small_caps_enabled:BOOLEAN
			-- `True' if `Current' has the lower case characters replaced by smaller variants of the capital characters.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_variant(item) = {PANGO_EXTERNAL}.pango_variant_small_caps
		end

	weight:INTEGER
			-- Specifies how bold or light `Current' should be
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_weight(item)
		end

	is_stretch_ultra_condensed:BOOLEAN
			-- Is `Current' with an ultra condensed streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_ultra_condensed
		end

	is_stretch_extra_condensed:BOOLEAN
			-- Is `Current' with an extra condensed streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_extra_condensed
		end

	is_stretch_condensed:BOOLEAN
			-- Is `Current' with a condensed streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_condensed
		end

	is_stretch_semi_condensed:BOOLEAN
			-- Is `Current' with a semi condensed streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_semi_condensed
		end

	is_stretch_normal:BOOLEAN
			-- Is `Current' with no streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_normal
		end

	is_stretch_semi_expanded:BOOLEAN
			-- Is `Current'  with a semi expanded streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_semi_expanded
		end

	is_stretch_expanded:BOOLEAN
			-- Is `Current'  with an expanded streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_expanded
		end

	is_stretch_extra_expanded:BOOLEAN
			-- Is `Current'  with an extra expanded streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_extra_expanded
		end

	is_stretch_ultra_expanded:BOOLEAN
			-- Is `Current'  with an ultra expanded streching
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_stretch(item) = {PANGO_EXTERNAL}.pango_stretch_ultra_expanded
		end

	size:INTEGER
			-- The size of `Current'
			-- If `is_absolute_size' is `True', this is in device unit
			-- If `is_absolute_size' is `False', this is in point
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_size(item)
		end

	is_absolute_size:BOOLEAN
			-- `True' if `Current' is in device units
			-- `False' if `Current' is in Pango units
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_size_is_absolute(item)
		end

	is_gravity_auto:BOOLEAN
			-- Is the gravity is manage by the {PANGO_CONTEXT} and not by `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_gravity(item) = {PANGO_EXTERNAL}.pango_gravity_auto
		end

	is_gravity_south:BOOLEAN
			-- Is the Glyphs stand upright
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_gravity(item) = {PANGO_EXTERNAL}.pango_gravity_south
		end

	is_gravity_east:BOOLEAN
			-- Is the Glyphs are rotated 90 degrees clockwise
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_gravity(item) = {PANGO_EXTERNAL}.pango_gravity_east
		end

	is_gravity_north:BOOLEAN
			-- Is the Glyphs are upside-down
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_gravity(item) = {PANGO_EXTERNAL}.pango_gravity_north
		end

	is_gravity_west:BOOLEAN
			-- Is the Glyphs are rotated 90 degrees counter-clockwise
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_get_gravity(item) = {PANGO_EXTERNAL}.pango_gravity_west
		end

feature -- Duplication

	copy (a_other: like Current)
			-- <Precursor>
		do
			dispose
			make_owned({PANGO_EXTERNAL}.pango_font_description_copy(a_other.item))
		end

feature -- Comparison

	is_equal (a_other: like Current): BOOLEAN
			-- Is `a_other' attached to an object considered
			-- equal to current object?
		do
			Result := {PANGO_EXTERNAL}.pango_font_description_equal(item, a_other.item)
		end

feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists and not shared then
				{PANGO_EXTERNAL}.pango_font_description_free (item)
			end
		end

invariant
	Weight_Valid: weight >= 100 and weight <= 1000

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
