note
	description: "General constants used with Eiffel Pango."
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

class
	PANGO_CONSTANTS

feature -- Access

	pango_scale:INTEGER
			-- Scale between dimensions used for Pango distances and
			-- device units. (The definition of device units is
			-- dependent on the output device; it will typically be
			-- pixels for a screen, and points for a printer.)
		once
			Result := {PANGO_EXTERNAL}.pango_scale
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
