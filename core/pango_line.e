note
	description: "Represent a line of text in a {PANGO_LAYOUT].."
	author: "Louis Marchand"
	date: "Sat, 30 Jun 2018 13:38:36 +0000"
	revision: "0.1"

class
	PANGO_LINE

inherit
	PANGO_MEMORY_STRUCTURE
		redefine
			dispose
		end
	PANGO_DIRECTIONNABLE
		rename
			valid as exists
		end
	PANGO_VISUALISABLE
		rename
			valid as exists
		end

create {PANGO_LAYOUT}
	make

feature {NONE} -- Initialization

	make(a_item:POINTER; a_layout:PANGO_LAYOUT)
			-- Initialization of `Current' using `a_item' as `item' and `a_layout' as `layout'
		do
			make_owned ({PANGO_EXTERNAL}.pango_layout_line_ref(a_item))
			layout := a_layout
		end

feature -- Access

	start_index:INTEGER
			-- The index in {PANGO_LAYOUT}.`text' at wich `Current' start
		require
			Exists: exists
		local
			l_c_start_index, l_start_index:INTEGER
			l_c_text:C_STRING
			l_utf:PANGO_UTF_CONVERTER
		do
			l_c_start_index := {PANGO_EXTERNAL}.PangoLayoutLine_start_index_get (item)
			create l_c_text.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
			l_start_index := l_utf.utf_8_string_8_to_string_32_count(l_c_text.string, 1, l_c_start_index) + 1
			Result := l_start_index
		end

	count: INTEGER
			-- The number of character in `Current'
		require
			Exists: exists
		local
			l_c_start_index, l_c_count, l_count:INTEGER
			l_c_text:C_STRING
			l_utf:PANGO_UTF_CONVERTER
		do
			l_c_start_index := {PANGO_EXTERNAL}.PangoLayoutLine_start_index_get (item)
			l_c_count := {PANGO_EXTERNAL}.PangoLayoutLine_length_get (item)
			create l_c_text.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
			l_count := l_utf.utf_8_string_8_to_string_32_count(l_c_text.string, l_c_start_index, l_c_count)
			Result := l_count
		end

	is_paragraph_start:BOOLEAN
			-- `Current' is the first line of the paragraph
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.PangoLayoutLine_is_paragraph_start_get (item)
		end

	grapheme_x(a_index:INTEGER; a_is_trailing:BOOLEAN):INTEGER
			-- `x' position of the grapheme at `a_index' of {PANGO_LAYOUT}.`text'.
			-- if `a_is_trailing' is `True', the `x' position is the trailing edge
			-- of the grapheme, and if `False', the leading of the grapheme.
			-- (`x' position is measured from the left edge of `Current')
		require
			Exists: exists
			Index_valid: a_index >= start_index and a_index <= start_index + count - 1
		local
			l_x, l_index:INTEGER
			l_utf:PANGO_UTF_CONVERTER
		do
			l_index := l_utf.utf_8_bytes_count (layout.text, 1, a_index)
			{PANGO_EXTERNAL}.pango_layout_line_index_to_x(item, l_index, a_is_trailing, $l_x)
			Result := l_x
		end



	grapheme_index(a_x:INTEGER):detachable TUPLE[index, trailing:INTEGER]
			-- From the logical horizontal position `a_x' in Pango units, the index
			-- of the grapheme. `is_trailing' will either be zero, or the number of
			-- characters in the grapheme. 0 represents the leading edge of the grapheme.
			-- `Void' if (`a_x',`a_y') is not in `text'.
			-- If `a_x' is outside `Current', `index' and `trailing' will point to the
			-- very first or very last position in `Current'.
		require
			Exists: exists
		local
			l_index, l_trailing:INTEGER
			l_result:BOOLEAN
			l_c_text:C_STRING
			l_utf:PANGO_UTF_CONVERTER
		do
			l_result := {PANGO_EXTERNAL}.pango_layout_line_x_to_index(item, a_x, $l_index, $l_trailing)
			if l_result then
				create l_c_text.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
				l_index := l_utf.utf_8_string_8_to_string_32_count(l_c_text.string, 1, l_index) + 1
				Result := [l_index, l_trailing]
			end
		end


	layout:PANGO_LAYOUT
			-- The {PANGO_LAYOUT} that `Current' has been created in


feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists and not shared then
				{PANGO_EXTERNAL}.pango_layout_line_unref (item)
			end
		end

feature {NONE} -- Implementation

	direction_index:INTEGER
			-- <Precursor>
		do
			Result := {PANGO_EXTERNAL}.PangoLayoutLine_resolved_dir_get (item).to_integer_32
		end

	logical_extents_routine:PROCEDURE[POINTER]
			-- <Precursor>
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_line_get_extents(item, null, ?)
		end

	ink_extents_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_line_get_extents(item, ?, null)
		end

	logical_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_line_get_pixel_extents(item, null, ?)
		end

	ink_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_line_get_pixel_extents(item, ?, null)
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
