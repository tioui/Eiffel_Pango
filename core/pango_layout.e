note
	description: "Represent a text area layout. This is the main high level class."
	author: "Louis Marchand"
	date: "Thu, 28 Jun 2018 16:18:54 +0000"
	revision: "0.1"
	ToDo: "pango_layout_get_log_attrs, pango_layout_get_iter"

class
	PANGO_LAYOUT

inherit
	PANGO_VISUALISABLE
		rename
			valid as exists
		redefine
			copy
		end
	PANGO_MEMORY_STRUCTURE
		redefine
			copy
		end

create
	make

feature {NONE} -- Initialization

	make(a_context:PANGO_CONTEXT)
			-- Initialization of `Current' using `a_context' as `context'
		do
			internal_context := a_context
			make_owned ({PANGO_EXTERNAL}.pango_layout_new(a_context.item))
		end

feature -- Access

	context:PANGO_CONTEXT
			-- The {PANGO_CONTEXT} used in `Current'
			-- Function not pure, it may modify `internal_context' as lazy loading
			-- Must be disposed if `Current' is disposed
		require
			Exists:exists
		do
			if attached internal_context as la_context then
				Result := la_context
			else
				create Result.make_from_layout(Current)
				internal_context := Result
			end
		end

	update_context
			-- To launch when the state of the `context' has changed and
			-- `Current' have to take those changes in consideration
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_context_changed(item)
		end

	serial_number:NATURAL
			-- Current serial number of `Current'. The serial number is
			-- initialized to an small number larger than zero when a new
			-- layout is created and is increased whenever the layout is changed
			-- using any of the setter functions, or the PangoContext it uses has changed.
			-- This can be used, for example, to decide whether `Current' needs redrawing.
			-- To force the serial to be increased, use `update_context'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_serial(item)
		end

	set_text(a_text:READABLE_STRING_GENERAL)
			-- Set the text of `Current'
			-- Note that if you have used set_text_with_markup before, you should
			-- `clear_attributes' first
		require
			Exists: exists
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_c_string.make (l_utf.string_32_to_utf_8_string_8 (a_text.to_string_32))
			{PANGO_EXTERNAL}.pango_layout_set_text(item, l_c_string.item, -1)
		ensure
			Is_Assign: text.to_string_32 ~ a_text.to_string_32
		end

	text:READABLE_STRING_GENERAL assign set_text
			-- Text of `Current'
		require
			Exists: exists
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_c_string.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
			Result := l_utf.utf_8_string_8_to_string_32 (l_c_string.string)
		end

	get_text_count:INTEGER
			-- The number of character in `text' (faster than getting `text')
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_character_count(item)
		end

	set_text_with_markup(a_text:READABLE_STRING_GENERAL)
			-- Set the text and attributes of `Current' using marup language
		require
			Exists: exists
		local
			l_utf:UTF_CONVERTER
			l_c_string:C_STRING
		do
			create l_c_string.make (l_utf.string_32_to_utf_8_string_8 (a_text.to_string_32))
			{PANGO_EXTERNAL}.pango_layout_set_markup(item, l_c_string.item, -1)
		end

	set_attributes(a_attributes:PANGO_ATTRIBUTES)
			-- Assign `a_attributes' to `attributes'
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_attributes(item, a_attributes.item)
		end

	attributes:PANGO_ATTRIBUTES assign set_attributes
			-- The attributes of `Current'
		require
			Exists: exists
		do
			create Result.make_from_item({PANGO_EXTERNAL}.pango_layout_get_attributes(item))
		end

	clear_attributes
			-- Remove every attributes of `Current'
		require
			Exists: exists
		do
			set_attributes(create {PANGO_ATTRIBUTES})
		end

	font_description:detachable PANGO_FONT_DESCRIPTION_READABLE assign set_font_description
			-- The description of the font
		require
			Exists: exists
		local
			l_pointer:POINTER
		do
			l_pointer := {PANGO_EXTERNAL}.pango_layout_get_font_description(item)
			if not l_pointer.is_default_pointer then
				create Result.make_shared (l_pointer)
			end
		end

	set_font_description(a_font_description: detachable PANGO_FONT_DESCRIPTION_READABLE)
			-- Assign `font_description' with the value of `a_font_description'
		require
			Exists: exists
		do
			if attached a_font_description as la_font_description then
				{PANGO_EXTERNAL}.pango_layout_set_font_description(item, a_font_description.item)
			else
				{PANGO_EXTERNAL}.pango_layout_set_font_description(item, null)
			end
		end

	disable_wrapping
			-- Disable the text wrapping. To enable wrapping, set a value
			-- to `width'
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_width(item, -1)
		end

	is_wrapping_enabled:BOOLEAN
			-- `True' if `Current' haveto wrap words of a paragraphe
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_width(item) /= -1
		end

	width:INTEGER assign set_width
			-- The horizontal dimention of the text area in Pango Units
		require
			Exists: exists
			Is_Wrapping_Disabled: not is_wrapping_enabled
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_width(item)
		end

	set_width(a_width:INTEGER)
			-- Assign `width' with the value of `a_width'
		require
			Exists: exists
			Width_Positive: a_width >= 0
		do
			{PANGO_EXTERNAL}.pango_layout_set_width(item, a_width)
		ensure
			Is_Assign: width ~ a_width
		end

	height:INTEGER
			-- Vertical dimention of the text area.
			-- The `Result' is in Pango Units if `is_height_units'
			-- is `True' and in number of lines if `is_height_units'
			-- is `False'. A value of 0 means only one line will be shown.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_height(item).abs
		end

	set_height_units(a_height:INTEGER)
			-- Assign `height' with the value of `a_height' in Pango Units.
			-- Only lines would be shown that would fit, and if there is any
			-- text omitted, an ellipsis added.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_height(item, a_height)
		ensure
			Is_Assign: height ~ a_height
			Is_Units: is_height_units
		end

	set_height_lines(a_height:INTEGER)
			-- Assign `height' with the value of `a_height' in number of lines
			-- A value of 0 means that only one line will be shown. A value of 1
			-- means that the first line of each paragraphe will be ellipsized.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_height(item, -a_height)
		ensure
			Is_Assign: height ~ a_height
			Is_Not_Units: not is_height_units
		end

	is_height_units:BOOLEAN
			-- `True' if `height' is set in Pango Units.
			-- `False' if `height' is set in number of lines.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_height(item) > 0
		end

	set_wrap_mode_word
			-- wrap lines at word boundaries
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_wrap(item, {PANGO_EXTERNAL}.pango_wrap_word)
		ensure
			Is_Set: is_wrap_mode_word
		end

	is_wrap_mode_word:BOOLEAN
			-- `True' if wrap lines at word boundaries
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_wrap(item) = {PANGO_EXTERNAL}.pango_wrap_word
		end

	set_wrap_mode_char
			-- wrap lines at character boundaries
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_wrap(item, {PANGO_EXTERNAL}.pango_wrap_char)
		ensure
			Is_Set: is_wrap_mode_char
		end

	is_wrap_mode_char:BOOLEAN
			-- `True' if wrap lines at character boundaries
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_wrap(item) = {PANGO_EXTERNAL}.pango_wrap_char
		end

	set_wrap_mode_word_or_char
			-- wrap lines at word boundaries, but fall back to character
			-- boundaries if there is not enough space for a full word.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_wrap(item, {PANGO_EXTERNAL}.pango_wrap_word_char)
		ensure
			Is_Set: is_wrap_mode_word_or_char
		end

	is_wrap_mode_word_or_char:BOOLEAN
			-- `True' if wrap lines at word boundaries, but fall back to character
			-- boundaries if there is not enough space for a full word.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_wrap(item) = {PANGO_EXTERNAL}.pango_wrap_word_char
		end

	has_wrapped:BOOLEAN
			-- `True' if `is_wrapping_enabled' is `True', if `is_ellipsization_enabled' is `False'
			-- and if paragraphs exceeding the layout width that have to be wrapped.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_is_wrapped(item)
		end

	disable_ellipsize
			-- Set `Current' to not ellipsize
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_ellipsize(item, {PANGO_EXTERNAL}.pango_ellipsize_none)
		ensure
			Is_Unset: not is_ellipsize_enabled
		end

	is_ellipsize_enabled:BOOLEAN
			-- `Current' don't ellipsize
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_ellipsize(item) /= {PANGO_EXTERNAL}.pango_ellipsize_none
		end

	set_ellipsize_start
			-- Set `Current' to omit characters at the start of the text
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_ellipsize(item, {PANGO_EXTERNAL}.pango_ellipsize_start)
		ensure
			Is_Unset: not is_ellipsize_start
		end

	is_ellipsize_start:BOOLEAN
			-- `True' if `Current' omit characters at the start of the text
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_ellipsize(item) = {PANGO_EXTERNAL}.pango_ellipsize_start
		end

	set_ellipsize_middle
			-- Set `Current' to omit characters at the middle of the text
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_ellipsize(item, {PANGO_EXTERNAL}.pango_ellipsize_middle)
		ensure
			Is_Unset: not is_ellipsize_middle
		end

	is_ellipsize_middle:BOOLEAN
			-- `True' if `Current' omit characters at the middle of the text
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_ellipsize(item) = {PANGO_EXTERNAL}.pango_ellipsize_middle
		end

	set_ellipsize_end
			-- Set `Current' to omit characters at the end of the text
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_ellipsize(item, {PANGO_EXTERNAL}.pango_ellipsize_end)
		ensure
			Is_Unset: not is_ellipsize_end
		end

	is_ellipsize_end:BOOLEAN
			-- `True' if `Current' omit characters at the end of the text
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_ellipsize(item) = {PANGO_EXTERNAL}.pango_ellipsize_end
		end

	has_ellipsize:BOOLEAN
			-- `True' if `is_ellipsize_enabled' is `True', a positive value is set to `width'
			-- and if there are paragraphs exceeding that width that have to be ellipsized
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_is_ellipsized(item)
		end

	indentation:INTEGER
			-- Width in Pango units to indent each paragraph
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_indent(item).abs
		end

	set_indentation(a_indentation:INTEGER)
			-- Assign `indentation'  with the value of `a_indentation'
		require
			Exists: exists
			Indentation_Positive: a_indentation >= 0
		do
			{PANGO_EXTERNAL}.pango_layout_set_indent(item, a_indentation)
		ensure
			Is_Assign: indentation ~ a_indentation
			Not_Hanging: not is_hanging_indentation
		end

	set_hanging_indentation(a_indentation:INTEGER)
			-- Assign `indentation'  with the value of `a_indentation' representing a hanging indentation.
			-- That is, the first line will have the full width, and subsequent lines will be indented
		require
			Exists: exists
			Indentation_Positive: a_indentation > 0
		do
			{PANGO_EXTERNAL}.pango_layout_set_indent(item, -a_indentation)
		ensure
			Is_Assign: indentation ~ a_indentation
			Is_Hanging: is_hanging_indentation
		end

	is_hanging_indentation:BOOLEAN
			-- The `indentation' represent a hanging indentation.
			-- That is, the first line will have the full width, and subsequent lines will be indented
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_indent(item) < 0
		end

	spacing:INTEGER assign set_spacing
			-- Amount of spacing between the lines of the layout.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_spacing(item)
		end

	set_spacing(a_spacing:INTEGER)
			-- Assign `spacing' with the value of `a_spacing'
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_spacing(item, a_spacing)
		ensure
			Is_Assign: spacing ~ a_spacing
		end

	is_justify_enabled:BOOLEAN
			-- `True' if each complete line should be stretched to fill the entire width of the layout.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_justify(item)
		end

	enable_justify
			-- Each complete line should be stretched to fill the entire width of the layout.
			-- This stretching is typically done by adding whitespace, but for some scripts (such as Arabic),
			-- the justification may be done in more complex ways, like extending the characters.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_justify(item, True)
		ensure
			Is_Enabled: is_justify_enabled
		end

	disable_justify
			-- Each complete line should not be stretched to fill the entire width of the layout.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_justify(item, False)
		ensure
			Is_Disabled: not is_justify_enabled
		end

	is_automatic_direction_enabled:BOOLEAN
			-- `True' if `Current' have to calculate the bidirectional base direction
			-- according to the contents of the layout.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_auto_dir(item)
		end

	enable_automatic_direction
			-- Calculate the bidirectional base direction according to the contents of the layout.
			-- It `True',  paragraphs in `Current' that begin with strong right-to-left characters
			-- (Arabic and Hebrew principally), will have right-to-left layout, paragraphs with
			-- letters from other scripts will have left-to-right layout.
			-- Paragraphs with only neutral characters get their direction from the surrounding paragraphs.
			-- When the auto-computed direction of a paragraph differs from the direction of the `context',
			-- the left alignment (`set_alignment_left', `is_alignment_left') and right alignment
			-- (`set_alignment_right', `is_alignment_right') are inversed
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_auto_dir(item, True)
		ensure
			Is_Enabled: is_automatic_direction_enabled
		end

	disable_automatic_direction
			-- Do not calculate the bidirectional base direction according to the contents of the layout.
			-- The direction will be set in the `context'
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_auto_dir(item, False)
		ensure
			Is_Disabled: not is_automatic_direction_enabled
		end

	is_alignment_left:BOOLEAN
			 -- `True' if `Current' put all available space on the right.
			 -- Automatic direction can impact on this setting (see: `enable_automatic_direction')
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_alignment(item) = {PANGO_EXTERNAL}.pango_align_left
		end

	set_alignment_left
			 -- Put all available space on the right
			 -- Automatic direction can impact on this setting (see: `enable_automatic_direction')
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_alignment(item, {PANGO_EXTERNAL}.pango_align_left)
		ensure
			Is_Set: is_alignment_left
		end

	is_alignment_right:BOOLEAN
			 -- `True' if `Current' put all available space on the left.
			 -- Automatic direction can impact on this setting (see: `enable_automatic_direction')
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_alignment(item) = {PANGO_EXTERNAL}.pango_align_right
		end

	set_alignment_right
			 -- Put all available space on the left
			 -- Automatic direction can impact on this setting (see: `enable_automatic_direction')
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_alignment(item, {PANGO_EXTERNAL}.pango_align_right)
		ensure
			Is_Set: is_alignment_right
		end

	is_alignment_center:BOOLEAN
			 -- `True' if `Current' put all available space on the left.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_alignment(item) = {PANGO_EXTERNAL}.pango_align_center
		end

	set_alignment_center
			 -- Put all available space on the left
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_alignment(item, {PANGO_EXTERNAL}.pango_align_center)
		ensure
			Is_Set: is_alignment_center
		end

	is_single_paragraph:BOOLEAN
			 -- `True' if `Current' do not treat newlines and similar characters as paragraph separators.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_single_paragraph_mode(item)
		end

	enable_single_paragraph
			 -- Do not treat newlines and similar characters as paragraph separators.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_single_paragraph_mode(item, True)
		ensure
			Is_Enabled: is_single_paragraph
		end

	disable_single_paragraph
			 -- Treat newlines and similar characters as paragraph separators.
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_layout_set_single_paragraph_mode(item, False)
		ensure
			Is_Disabled: not is_single_paragraph
		end

	unknown_glyphs_count:INTEGER
			-- Counts the number unknown glyphs in `text'
			-- This function can be used to determine if there are any fonts
			-- available to render all characters in a certain `text'.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_unknown_glyphs_count(item)
		end

	tab_list:detachable PANGO_TAB_LIST assign set_tab_list
			-- The tab locations used in `Current'
			-- If `Void', 8 tabs are used
		require
			Exists: exists
		local
			l_list:POINTER
		do
			l_list := {PANGO_EXTERNAL}.pango_layout_get_tabs(item)
			if not l_list.is_default_pointer then
				create Result.from_internal_item ({PANGO_EXTERNAL}.pango_layout_get_tabs(item))
			end
		end

	set_tab_list(a_tab_list:detachable PANGO_TAB_LIST)
			-- Assign `tab_list' with the value of `a_tab_list'
		require
			Exists: exists
		do
			if attached a_tab_list as la_list then
				{PANGO_EXTERNAL}.pango_layout_set_tabs(item, la_list.internal_item)
			else
				{PANGO_EXTERNAL}.pango_layout_set_tabs(item, null)
			end
		end

	grapheme_boundary(a_index:INTEGER):TUPLE[x, y, width, height:INTEGER]
			-- Boundary of the grapheme at `a_index' of `text'.
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= text.count
		local
			l_index:INTEGER
			l_external:PANGO_EXTERNAL
			l_utf:PANGO_UTF_CONVERTER
		do
			l_index := l_utf.utf_8_bytes_count (text, 1, a_index)
			Result := launch_for_rectangle(agent l_external.pango_layout_index_to_pos(item, l_index, ?))
		end

	grapheme_line_and_x(a_index:INTEGER; a_is_trailing:BOOLEAN):TUPLE[line, x:INTEGER]
			-- `Line' and `x' position of the grapheme at `a_index' of `text'.
			-- if `a_is_trailing' is `True', the `x' position is the trailing edge
			-- of the grapheme, and if `False', the leading of the grapheme.
			-- (`x' position is measured from the left edge of the line)
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= text.count
		local
			l_line, l_x, l_index:INTEGER
			l_utf:PANGO_UTF_CONVERTER
		do
			l_index := l_utf.utf_8_bytes_count (text, 1, a_index)
			{PANGO_EXTERNAL}.pango_layout_index_to_line_x(item, l_index, a_is_trailing, $l_line, $l_x)
			Result := [l_line, l_x]
		end

	grapheme_index(a_x, a_y:INTEGER):detachable TUPLE[index, trailing:INTEGER]
			-- From the logical position (`a_x', `a_y') in Pango units, the index
			-- of the grapheme. `is_trailing' will either be zero, or the number of
			-- characters in the grapheme. 0 represents the leading edge of the grapheme.
			-- `Void' if (`a_x',`a_y') is not in `text'
		require
			Exists: exists
		local
			l_index, l_trailing:INTEGER
			l_result:BOOLEAN
			l_c_text:C_STRING
			l_utf:PANGO_UTF_CONVERTER
		do
			l_result := {PANGO_EXTERNAL}.pango_layout_xy_to_index(item, a_x, a_y, $l_index, $l_trailing)
			if l_result then
				create l_c_text.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
				l_index := l_utf.utf_8_string_8_to_string_32_count(l_c_text.string, 1, l_index) + 1
				Result := [l_index, l_trailing]
			end
		end

	strong_cursor_position(a_index:INTEGER): TUPLE[x, y, height:INTEGER]
			-- Location where characters of the directionality equal
			-- to the base direction of `Current' are inserted.
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= text.count
		local
			l_index:INTEGER
			l_utf:PANGO_UTF_CONVERTER
			l_tuple:TUPLE[x, y, width, height:INTEGER]
			l_external:PANGO_EXTERNAL
		do
			l_index := l_utf.utf_8_bytes_count (text, 1, a_index)
			l_tuple := launch_for_rectangle(agent l_external.pango_layout_get_cursor_pos(item, l_index, ?, null))
			Result := [l_tuple.x, l_tuple.y, l_tuple.height]
		end

	week_cursor_position(a_index:INTEGER): TUPLE[x, y, height:INTEGER]
			-- location where characters of the directionality opposite
			-- to the base direction of `Current' are inserted.
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= text.count
		local
			l_index:INTEGER
			l_tuple:TUPLE[x, y, width, height:INTEGER]
			l_utf:PANGO_UTF_CONVERTER
			l_external:PANGO_EXTERNAL
		do
			l_index := l_utf.utf_8_bytes_count (text, 1, a_index)
			l_tuple := launch_for_rectangle(agent l_external.pango_layout_get_cursor_pos(item, l_index, null, ?))
			Result := [l_tuple.x, l_tuple.y, l_tuple.height]
		end

	move_strong_cursor_left(a_source_index, a_source_trailing, a_position:INTEGER):TUPLE[index, trailing:INTEGER]
			-- Get the `index' and `trailing' of the strong cursor after visual cursor move
			-- of `a_position' to the left considering that the cursor is at `a_source_index' and
			-- use a `a_source_traiing'.
		require
			Exists: exists
			Source_Index_Valid: a_source_index >= 1 and a_source_index <= text.count
			Position_Valid: a_position >= 0
		do
			Result := move_cursor (a_source_index, a_source_trailing, -a_position, True)
		ensure
			Index_Valid: Result.index >= 1 and Result.index <= text.count
		end

	move_strong_cursor_right(a_source_index, a_source_trailing, a_position:INTEGER):TUPLE[index, trailing:INTEGER]
			-- Get the `index' and `trailing' of the strong cursor after visual cursor move
			-- of `a_position' to the right considering that the cursor is at `a_source_index' and
			-- use a `a_source_traiing'.
		require
			Exists: exists
			Source_Index_Valid: a_source_index >= 1 and a_source_index <= text.count
			Position_Valid: a_position >= 0
		do
			Result := move_cursor (a_source_index, a_source_trailing, a_position, True)
		ensure
			Index_Valid: Result.index >= 1 and Result.index <= text.count
		end

	move_weak_cursor_left(a_source_index, a_source_trailing, a_position:INTEGER):TUPLE[index, trailing:INTEGER]
			-- Get the `index' and `trailing' of the strong cursor after visual cursor move
			-- of `a_position' to the left considering that the cursor is at `a_source_index' and
			-- use a `a_source_traiing'.
		require
			Exists: exists
			Source_Index_Valid: a_source_index >= 1 and a_source_index <= text.count
			Position_Valid: a_position >= 0
		do
			Result := move_cursor (a_source_index, a_source_trailing, -a_position, False)
		ensure
			Index_Valid: Result.index >= 1 and Result.index <= text.count
		end

	move_weak_cursor_right(a_source_index, a_source_trailing, a_position:INTEGER):TUPLE[index, trailing:INTEGER]
			-- Get the `index' and `trailing' of the strong cursor after visual cursor move
			-- of `a_position' to the right considering that the cursor is at `a_source_index' and
			-- use a `a_source_traiing'.
		require
			Exists: exists
			Source_Index_Valid: a_source_index >= 1 and a_source_index <= text.count
			Position_Valid: a_position >= 0
		do
			Result := move_cursor (a_source_index, a_source_trailing, a_position, False)
		ensure
			Index_Valid: Result.index >= 1 and Result.index <= text.count
		end

	size:TUPLE[width, height:INTEGER]
			-- The `width' and `hight' of `Current' in Pango units
		require
			Exists: exists
		local
			l_width, l_height:INTEGER
		do
			{PANGO_EXTERNAL}.pango_layout_get_size(item, $l_width, $l_height)
			Result := [l_width, l_height]
		end

	size_pixels:TUPLE[width, height:INTEGER]
			-- The `width' and `hight' of `Current' in device units
		require
			Exists: exists
		local
			l_width, l_height:INTEGER
		do
			{PANGO_EXTERNAL}.pango_layout_get_pixel_size(item, $l_width, $l_height)
			Result := [l_width, l_height]
		end

	baseline:INTEGER
			--  Y position of baseline of the first line in `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_baseline(item)
		end

	line_count:INTEGER
			--  The number of `line' in `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_layout_get_line_count(item)
		end

	line(a_index:INTEGER):PANGO_LINE
			-- The `a_index'-th line of `Current'
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= line_count
		do
			create Result.make ({PANGO_EXTERNAL}.pango_layout_get_line_readonly(item, a_index - 1), Current)
		end

feature -- Duplication

	copy (a_other: like Current)
			-- <Precursor>
		do
			internal_context := a_other.internal_context
			dispose
			make_owned({PANGO_EXTERNAL}.pango_layout_copy(a_other.item))
		end


feature {PANGO_LAYOUT} -- Implementation

	internal_context:detachable PANGO_CONTEXT
			-- Internal value of `context'


feature {NONE} -- Implementation

	logical_extents_routine:PROCEDURE[POINTER]
			-- <Precursor>
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_get_extents(item, null, ?)
		end

	ink_extents_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_get_extents(item, ?, null)
		end

	logical_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_get_pixel_extents(item, null, ?)
		end

	ink_extents_pixels_routine:PROCEDURE[POINTER]
			-- The routine to use to get `logical_extents'
		local
			l_external:PANGO_EXTERNAL
		do
			Result := agent l_external.pango_layout_get_pixel_extents(item, ?, null)
		end

	move_cursor(a_source_index, a_source_trailing, a_position:INTEGER; a_strong:BOOLEAN):TUPLE[index, trailing:INTEGER]
			-- Internal method used for all move_*_cursor_* methods
		require
			Exists: exists
			Source_Index_Valid: a_source_index >= 1 and a_source_index <= text.count
			Position_Valid: a_position >= 0
		local
			l_source_index, l_index, l_trailing:INTEGER
			l_utf:PANGO_UTF_CONVERTER
			l_c_text:C_STRING
		do
			l_source_index := l_utf.utf_8_bytes_count (text, 1, a_source_index)
			{PANGO_EXTERNAL}.pango_layout_move_cursor_visually(item, a_strong,
														a_source_index, l_source_index,
														a_position, $l_index, $l_trailing)
			create l_c_text.make_shared_from_pointer ({PANGO_EXTERNAL}.pango_layout_get_text(item))
			l_index := l_utf.utf_8_string_8_to_string_32_count(l_c_text.string, 1, l_index)
			Result := [l_index, l_trailing]
		ensure
			Index_Valid: Result.index >= 1 and Result.index <= text.count
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
