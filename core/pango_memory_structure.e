note
	description: "Use as ancestor for some {PANGO_*} classes that use internal C pointer."
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"

deferred class
	PANGO_MEMORY_STRUCTURE

inherit
	PANGO_ANY
	DISPOSABLE

feature {NONE} -- Initialization

	make_owned(a_item:POINTER)
			-- Initialization of `Current' using `a_item' as `item'
			-- Item will be freed on `dispose'
		do
			item := a_item
			shared := False
		ensure
			Item_Valid: item ~ a_item
			Not_Shared: not shared
		end

	make_shared(a_item:POINTER)
			-- Initialization of `Current' using `a_item' as `item'
			-- Item will not be freed on `dispose'
		do
			item := a_item
			shared := True
		ensure
			Item_Valid: item ~ a_item
			Shared: shared
		end

feature -- Access

	exists:BOOLEAN
			-- `Current' is valid
		do
			Result := not item.is_default_pointer
		end


feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists and not shared then
				{PANGO_EXTERNAL}.g_object_unref (item)
			end
		end

feature {PANGO_ANY} -- Implementation

	item:POINTER
			-- Internal representation of `Current'

	shared:BOOLEAN
			-- `False' if `Current' should be freed on `dispose'
invariant

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
