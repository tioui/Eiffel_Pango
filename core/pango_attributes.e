note
	description: "Use to store text attributes (Not functionnal yet)"
	author: "Louis Marchand"
	date: "Sun, 24 Jun 2018 20:27:06 +0000"
	revision: "0.1"
	ToDo: "Adding attributes"

class
	PANGO_ATTRIBUTES

inherit
	PANGO_MEMORY_STRUCTURE
		redefine
			default_create, dispose, copy
		end

create
	default_create

create {PANGO_ANY}
	make_from_item

feature {NONE} --Initialization

	default_create
			-- Initialization of `Current'
		do
			make_owned ({PANGO_EXTERNAL}.pango_attr_list_new)
		end

	make_from_item(a_item:POINTER)
			-- Initialization of `Current' using `a_item' as `item'
			-- Reference `a_item'
		do
			make_owned ({PANGO_EXTERNAL}.pango_attr_list_ref(a_item))
		end


feature -- Access


feature -- Duplication

	copy (a_other: attached PANGO_ATTRIBUTES)
			-- <Precursor>
		do
			dispose
			item := {PANGO_EXTERNAL}.g_list_copy(a_other.item)
		end

feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists and not shared then
				{PANGO_EXTERNAL}.pango_attr_list_unref (item)
			end
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
