note
	description: "Stores global information used to control the itemization process."
	author: "Louis Marchand"
	date: "Tue, 22 May 2018 14:15:16 +0000"
	revision: "0.1"

class
	PANGO_CONTEXT

inherit
	PANGO_MEMORY_STRUCTURE
	PANGO_DIRECTIONNABLE
		rename
			valid as exists
		end

create
	make_from_font_map

create {PANGO_ANY}
	make_shared,
	make_owned

create {PANGO_LAYOUT}
	make_from_layout

feature {NONE} -- Initialization

	make_from_font_map(a_font_map:PANGO_FONT_MAP)
			-- Initialization of `Current' from `a_font_map'
		do
			shared_font_map := a_font_map
			make_owned({PANGO_EXTERNAL}.pango_font_map_create_context(a_font_map.item))
		end

	make_from_layout(a_layout:PANGO_LAYOUT)
			-- Initialization of `Current' comming from `a_layout
		do
			creating_layout := a_layout
			make_shared({PANGO_EXTERNAL}.pango_layout_get_context(a_layout.item))
		end


feature -- Access

	font_map:PANGO_FONT_MAP
			-- The font map of `Current'
		require
			Exists: exists
		do
			if attached shared_font_map as la_font_map then
				Result := la_font_map
			else
				create Result.make_shared({PANGO_EXTERNAL}.pango_context_get_font_map(item))
			end
		end

	set_direction_left_to_right
			-- Use a strong left-to-right direction
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_context_set_base_dir(item, {PANGO_EXTERNAL}.pango_direction_ltr)
		ensure
			Is_Set: is_direction_left_to_right
		end

	set_direction_right_to_left
			-- Use a strong right-to-left direction
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_context_set_base_dir(item, {PANGO_EXTERNAL}.pango_direction_rtl)
		ensure
			Is_Set: is_direction_right_to_left
		end

	set_direction_weak_left_to_right
			-- Use a weak left-to-right direction
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_context_set_base_dir(item, {PANGO_EXTERNAL}.pango_direction_weak_ltr)
		ensure
			Is_Set: is_direction_weak_left_to_right
		end

	set_direction_weak_right_to_left
			-- Use a weak right-to-left direction
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_context_set_base_dir(item, {PANGO_EXTERNAL}.pango_direction_weak_rtl)
		ensure
			Is_Set: is_direction_weak_right_to_left
		end

	set_direction_neutral
			-- Do not specifie a direction
		require
			Exists: exists
		do
			{PANGO_EXTERNAL}.pango_context_set_base_dir(item, {PANGO_EXTERNAL}.pango_direction_neutral)
		ensure
			Is_Set: is_direction_neutral
		end

feature {NONE} -- Implementation

	shared_font_map:detachable PANGO_FONT_MAP
			-- Internal shared representation of `font_map'

	creating_layout:detachable PANGO_LAYOUT
			-- The {PANGO_LAYOUT} that have created `Current'

	direction_index:INTEGER
			-- The PangoDirection internal index
		do
			Result := {PANGO_EXTERNAL}.pango_context_get_base_dir(item)
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
