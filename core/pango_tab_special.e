note
	description: "An internal list for {PANGO_TAB_LIST}"
	author: "Louis Marchan"
	date: "Thu, 28 Jun 2018 16:18:54 +0000"
	revision: "0.1"

class
	PANGO_TAB_SPECIAL

inherit
	DISPOSABLE
		redefine
			copy
		end

create {PANGO_TAB_LIST}
	make,
	make_pixels,
	make_from_item

feature {NONE} -- Initialization

	make(a_initial_count:INTEGER)
			-- Initialization of `Current' with an initial `count' of `a_initial_count'.
			-- `Current' use Pango units for mesuring tabs
		require
			a_initial_count > 0
		do
			item := {PANGO_EXTERNAL}.pango_tab_array_new(a_initial_count, False)
		end

	make_pixels(a_initial_count:INTEGER)
			-- Initialization of `Current' with an initial `count' of `a_initial_count'.
			-- `Current' use device units for mesuring tabs
		require
			a_initial_count > 0
		do
			item := {PANGO_EXTERNAL}.pango_tab_array_new(a_initial_count, True)
		end


	make_from_item(a_item:POINTER)
			-- Initialization of `Current' using `a_item' as `item'
		do
			item := a_item
		end

feature {PANGO_TAB_LIST, PANGO_TAB_SPECIAL} -- Access

	item:POINTER
			-- The internal representation of `Current'

	exists:BOOLEAN
			-- `Current' has a memory value
		do
			Result := not item.is_default_pointer
		end

	count: INTEGER
			-- The capacoty of `Current'
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_tab_array_get_size(item)
		end

	grow
			-- Double de `count' of `Current'
		require
			Exists: exists
		do
			resize(count * 2)
		end

	resize(a_count:INTEGER)
			-- Change te `count' to exactly `a_size'
		require
			Exists: exists
			Count_positive: a_count > 0
		do
			{PANGO_EXTERNAL}.pango_tab_array_resize(item, a_count)
		end

	at(a_index:INTEGER):INTEGER
			-- Reteive the tab location at `a_index'
		require
			Exists: exists
			Index_Valid: a_index >= 0 and a_index < count
		local
			l_item:INTEGER
		do
			{PANGO_EXTERNAL}.pango_tab_array_get_tab(item, a_index, null, $l_item)
			Result :=  l_item
		end

	put(a_index, a_location:INTEGER)
			-- Put the tab location `a_location' at `a_index' in `Current'
		require
			Exists: exists
			Index_Valid: a_index >= 0 and a_index < count
		do
			{PANGO_EXTERNAL}.pango_tab_array_set_tab(item, a_index, {PANGO_EXTERNAL}.pango_tab_left, a_location)
		end

	move_data(a_from_index, a_to_index, a_number:INTEGER)
			-- Move every data from `a_from_index' to `a_to_index'
			-- of `a_number' position.
		require
			Index_Valid_Positive: a_number >= 0 implies (
									a_from_index >= 0 and
									a_to_index + a_number < count
								)
			Index_Valid_Negative: a_number < 0 implies (
									a_from_index + a_number >= 0 and
									a_to_index < count
								)
		local
			l_index, l_start_index, l_end_index, l_incrementation:INTEGER
		do
			if a_number >= 0 then
				l_start_index := a_to_index
				l_end_index := a_from_index
				l_incrementation := 1
			else
				l_start_index := a_from_index
				l_end_index := a_to_index
				l_incrementation := -1
			end
			from
				l_index := l_start_index
			until
				l_index = a_to_index
			loop
				put (l_index + a_number, at (l_index))
				l_index := l_index - l_incrementation
			end
		end

	is_pixels:BOOLEAN
			-- `True' if the data of `Current' are in Pixels;
			-- `False' if the data of `Current' are in Pango Units.
		require
			Exists: exists
		do
			Result := {PANGO_EXTERNAL}.pango_tab_array_get_positions_in_pixels(item)
		end

feature -- Duplication

	copy (a_other: attached PANGO_TAB_SPECIAL)
			-- <Precursor>
		do
			dispose
			item := {PANGO_EXTERNAL}.pango_tab_array_copy(a_other.item)
		end

feature {NONE} -- Removal

	dispose
			-- <Precursor>
		do
			if exists then
				{PANGO_EXTERNAL}.pango_tab_array_free(item)
			end
		end


feature {NONE} -- Implementation

	null:POINTER
			-- A NULL C pointer
		once
			create Result
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"

end
