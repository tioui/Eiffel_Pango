note
	description: "A list of tab location (may be in Pango Units or in Pixels."
	author: "Louis Marchand"
	date: "Thu, 28 Jun 2018 16:18:54 +0000"
	revision: "0.1"

class
	PANGO_TAB_LIST

inherit
	DYNAMIC_LIST [INTEGER]
		redefine
			default_create, copy, extendible, prunable, replaceable,
			writable, readable
		end
	PANGO_ANY
		undefine
			is_equal
		redefine
			default_create, copy
		end


create {PANGO_ANY}
	from_internal_item

create
	default_create,
	make,
	make_pixels

feature {NONE} -- Initializtion

	from_internal_item(a_internal_item:POINTER)
			-- Initialization of `Current' using
			-- `a_internal_item' as internal list
		do
			create area.make_from_item (a_internal_item)
			initialization
			count := area.count
		ensure
			Is_Assign: internal_item = a_internal_item
			Index_Valid: index = 1
		end

	default_create
			-- Initialization of `Current' with an initial capacity of 1
		do
			make(1)
		ensure then
			Not_Pixels: not is_pixels
			Index_Valid: index = 1
			Count_Valid: count = 0
		end

	make(a_initial_capacity:INTEGER)
			-- Initialization of `Current' using an
			-- initial capacity of `a_initial_capacity'
			-- `a_initial_capacity' is used as an optimisation.
			-- The position of the tabs are in Pango Units
		do
			create area.make(a_initial_capacity)
			initialization
		ensure
			Not_Pixels: not is_pixels
			Index_Valid: index = 1
			Count_Valid: count = 0
		end

	make_pixels(a_initial_capacity:INTEGER)
			-- Initialization of `Current' using an
			-- initial capacity of `a_initial_capacity'
			-- `a_initial_capacity' is used as an optimisation.
			-- The position of the tabs are in pixels
		do
			create area.make(a_initial_capacity)
			initialization
		ensure
			Is_Pixels: is_pixels
			Index_Valid: index = 1
			Count_Valid: count = 0
		end

	initialization
			-- Common feature of every creator of `Current'
		do
			index := 1
			count := 0
		ensure
			Index_Valid: index = 1
			Count_Valid: count = 0
		end

feature -- Access

	cursor: PANGO_LIST_CURSOR
			-- <Precursor>
		do
			create Result.make(index)
		end

	index: INTEGER_32
			-- <Precursor>

	item:INTEGER
			-- <Precursor>
		do
			Result := area.at (index - 1)
		end

feature -- Element change

	extend (a_value: like item)
			-- Add a new occurrence of `a_value'.
		do
			if count >= area.count then
				area.grow
			end
			area.put (count, a_value)
			count := count + 1
		end

	put_right (a_value: like item)
			-- Add `a_value' to the right of cursor position.
			-- Do not move cursor.
		do
			if index = count then
				extend(a_value)
			else
				if count >= area.count then
					area.grow
				end
				area.move_data (index, count-1, 1)
				area.put (index, a_value)
				count := count + 1
			end
		end

	put_front (a_value: like item)
			-- Add `a_value` at beginning.
			-- Do not move cursor.
		do
			if count >= area.count then
				area.grow
			end
			area.move_data (0, count-1, 1)
			area.put (0, a_value)
			if not before then
				index := index + 1
			end
		end

feature -- Element change

	replace (a_value: like item)
			-- Replace current item by `a_value'.
		do
			area.put (index - 1, a_value)
		end

feature -- Measurement

	count:INTEGER
			-- <Precursor>

feature -- Cursor movement

	back
			-- <Precursor>
		do
			index := index - 1
		end

	forth
			-- <Precursor>
		do
			index := index + 1
		end

	go_to (p: CURSOR)
			-- <Precursor>
		do
			if attached {PANGO_LIST_CURSOR} p as al_c then
				index := al_c.index
			else
				check
					correct_cursor_type: False
				end
			end
		end

feature -- Status report

	is_pixels:BOOLEAN
			-- `True' if the data of `Current' are in Pixels;
			-- `False' if the data of `Current' are in Pango Units.
		require
			Exists: exists
		do
			Result := area.is_pixels
		end

	exists:BOOLEAN
			-- `Current' has a memory value
		do
			Result := area.exists
		end

	readable: BOOLEAN
			-- <Precursor>
		do
			Result := exists and Precursor
		end

	writable: BOOLEAN
			-- <Precursor>
		do
			Result := exists and Precursor
		end

	extendible: BOOLEAN
			-- May new items be added?
		do
			Result := exists
		end

	prunable: BOOLEAN
			-- May items be removed?
		do
			Result := exists
		end

	replaceable: BOOLEAN
			-- <Precursor>
		do
			Result := exists
		end

	full: BOOLEAN = False
			-- <Precursor>

	valid_cursor (p: CURSOR): BOOLEAN
			-- <Precursor>
		do
			if attached {PANGO_LIST_CURSOR} p as al_c then
				Result := valid_cursor_index (al_c.index)
			end
		end

feature -- Removal

	remove
			-- <Precursor>
		do
			area.move_data(index, count - 1, -1)
			count := count - 1
		end

	remove_left
			-- <Precursor>
		do
			index := index - 1
			remove
		end

	remove_right
			-- <Precursor>
		do
			index := index + 1
			remove
			index := index - 1
		end

feature -- Duplication

	copy (a_other: attached PANGO_TAB_LIST)
			-- <Precursor>
		do
			index := a_other.index
			count := a_other.count
			if a_other.area.exists then
				area := a_other.area.twin
			else
				area := a_other.area
			end
		end


feature {PANGO_TAB_LIST} -- Implementation

	new_chain: like Current
			-- <Precursor>
		do
			create Result
		end

	area:PANGO_TAB_SPECIAL
			-- The internal memory containing data of `Current'

feature {PANGO_ANY} -- Implementation

	internal_item:POINTER
			-- Internal representation of `Current'
			-- Need to be recall if any modification
			-- to `Current' occured
		do
			if area.exists then
				area.resize (count)
			end
			Result := area.item
		end

note
	copywrite: "Copyright (c) 2018, Louis Marchand"
	license: "[
			This library is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 2 of the License, or (at your option) any later version.

			This library is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Library General Public License for more details.
			
			You should have received a copy of the GNU Lesser General Public
			License along with this library; if not, see <http://www.gnu.org/licenses/>.
		]"


end
